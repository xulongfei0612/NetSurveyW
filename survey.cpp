#include "survey.h"

#ifdef __cplusplus
extern "C" 
#endif
	FILE _iob[3] = {__iob_func()[0], __iob_func()[1], __iob_func()[2]};
survey::survey(void)
{
	_captor=0;
	_auditor=0;
	_ringfifo=0;
	_run_flag=0;
	_off_captor=0;
}


survey::~survey(void)
{
	destroy();
}
int survey::create(){
	int ret=0;
	
	ret=_log.load_config(LOG_CONFIG_FILE);
	if(ret<=0){
		printf("log loadconfig failed.\n");
		return -1;
	}
	ret=_log.open_logger();
	if(ret<=0){
		printf("log open failed.\n");
		return -1;
	}

	if(_ringfifo==0){
		_ringfifo=new my_ringbuffbased_fifo<tcp_connection_info >;
		if(_ringfifo){
			if((ret=_ringfifo->init())!=0){
				_log.error("survey: ringbuff init failed.\n");
				return -1;
			}
			
		}
		else{
			return -1;
		}		
	}
	if(_captor==0){
		_captor=httpcaptor::get_instance();
		if(_captor){
			ret=_captor->load_config(CAPTOR_CONFIG_FILE);
			if(ret<0){
				_log.error("survey: captor load config failed.\n");
				return -1;
			}
			//ret=_captor->init();
			if(ret<0){
				_log.error("survey: captor init failed.\n");
				return -1;
			}
			_captor->setLogger(&_log);
			_captor->set_cached_ringfifo(_ringfifo);
		}
		else{
			return -1;
		}
	}
	if(_off_captor==0){
		_off_captor=offlinecaptor::get_instance();
		if(_off_captor){
			//ret=_off_captor->load_config(CAPTOR_CONFIG_FILE);
			//if(ret<0){
			//	_log.error("survey: captor load config failed.\n");
			//	return -1;
			//}
			ret=_off_captor->init();
			if(ret<0){
				_log.error("survey: captor init failed.\n");
				return -1;
			}
			_off_captor->setLogger(&_log);
			_off_captor->set_cached_ringfifo(_ringfifo);
		}
		else{
			return -1;
		}
	}
	if(_auditor==0){
		_auditor=new httpauditor;
		if(_auditor){
			ret=_auditor->create();
			if(ret<0){
				return -1;
			}
			_auditor->setLogger(&_log);
			_auditor->set_cached_ringfifo(_ringfifo);
		}
		else{
			return -1;
		}
	}
	return 0;
}
int survey::destroy(){

	if(_auditor){
		delete _auditor;
		_auditor=0;
	}
	httpcaptor::destroy_instance();
	offlinecaptor::destroy_instance();
	if(_ringfifo){
		delete _ringfifo;
		_ringfifo=0;
	}
	_log.close_logger();
	
	return 0;
}
int survey::load_config(){
	return 0;
}

int survey:: run(){
	int ret=0;
	if(!_captor){
		//ACE_DEBUG((LM_ERROR,"survey: captor not build.\n"));
		_log.error("survey: captor not build.\n");
		return -1;
	}
	if(!_auditor){
		//ACE_DEBUG((LM_ERROR,"survey: auditor not build.\n"));
		_log.error("survey: auditor not build.\n");
		return -1;
	}
	_ringfifo->enabled();
	_ringfifo->clear();
	printf("task begin...\n");
	Sleep(50);
	ret=_captor->activate();
	ret=_auditor->activate();
	_run_flag=1;
	//ACE_Thread_Manager::instance()->spawn((ACE_THR_FUNC)process_usr_quit,this);
	ACE_Reactor::instance()->run_reactor_event_loop();
	//ACE_Thread_Manager::instance()->wait();
	printf("task done...\n");
	return 0;

}
int  survey::stop(){
	if(_run_flag){
		_ringfifo->disabled();
		nids_breakloop();
		ACE_Thread_Manager::instance()->cancel_task(_captor);
		ACE_Thread_Manager::instance()->cancel_task(_auditor);
		_log.close_logger();
		ACE_Reactor::instance()->end_reactor_event_loop();
		_run_flag=0;
	}
	return 0;
}
int survey::run_offline(const char* file){
	int ret=0;
	if(!_off_captor){
		//ACE_DEBUG((LM_ERROR,"survey: captor not build.\n"));
		_log.error("survey: offline_captor not build.\n");
		return -1;
	}
	if(!_auditor){
		//ACE_DEBUG((LM_ERROR,"survey: auditor not build.\n"));
		_log.error("survey: auditor not build.\n");
		return -1;
	}
	_off_captor->set_offline_file(file);
	_ringfifo->enabled();
	_ringfifo->clear();
	printf("task begin...\n");
	Sleep(50);
	ret=_auditor->activate();
	ret=_off_captor->activate();
	_run_flag=1;
	//ACE_Thread_Manager::instance()->spawn((ACE_THR_FUNC)process_usr_quit,this);
	ACE_Reactor::instance()->run_reactor_event_loop();
	//ACE_Thread_Manager::instance()->wait();
	printf("task done...\n");
	return 0;
}
int survey::stop_offline(){
	if(_run_flag){
		_ringfifo->disabled();
		ACE_Thread_Manager::instance()->cancel_task(_off_captor);
		ACE_Thread_Manager::instance()->cancel_task(_auditor);
		_log.close_logger();
		ACE_Reactor::instance()->end_reactor_event_loop();
		_run_flag=0;
	}
	return 0;
}
std::vector<MatchResult> survey::run_regex(const char source[]){
	return _pcre.MatchAllRule(source);
}
int survey::add_rule(const std::string &name, const std::string &patten){
	return _pcre.AddRule(name,patten);
}
void survey::clear_rule(){
	_pcre.ClearRules();
}

void survey::process_usr_quit(void* args){
	survey* pthis=(survey*)args;
	if(!pthis){
		return ;
	}
	Sleep(100);
	while(1){
		ACE_DEBUG((LM_INFO,"type in 'q' to quit system\n"));
		char c=getchar();
		if (c=='q')
		{
			pthis->stop();
			break;
		}
		else
			continue;
		Sleep(50);
	}
}

void survey::register_callback(SESSION_CALLBACK p){
	if(_auditor){
		_auditor->register_callback(p);
	}
}
void survey::register_interaction_callback(INTERACTION_CALLBACK p){
	if(_auditor){
		_auditor->register_interaction_callback(p);
	}
}

int survey::reset(){
	if(_run_flag){
		stop();
		_ringfifo->clear();
		_auditor->reset();
	}
	return 0;
}