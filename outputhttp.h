#pragma once
#include <iostream>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include "msgdefine.h"
#include <string>
#include <time.h>
#include <errno.h>


#include <ace/Log_Msg.h>
#include <ace/OS.h>
#include <ace/Configuration.h>
#include <ace/Configuration_Import_Export.h>


#define HTTP_LOG_CONF_SECTION   "HTTPLOG"
#define HTTP_LOG_CONF_OUTDIR                 "OUTDIR"
#define HTTP_LOG_CONF_FILETYPE               "FILETYPE"

class outputhttp
{
public:
	outputhttp(void);
	~outputhttp(void);
	int load_config(const char * _config_filename);
	int create();
	int output(struct http_interaction* hi);
	int output(struct http_session* hs);
	int destroy();

private:
	BOOL FindFirstFileExists(LPCTSTR lpPath, DWORD dwFilter);
	BOOL FilePathExists(LPCTSTR lpPath);
	BOOL FolderExists(LPCTSTR lpPath);

private:
	std::ofstream  _out;
	FILE* _fout;
	std::string _outdir;
	char parentdir_[255];

};

