#pragma once
#include "ace/Task.h"  
#include <Windows.h>
#include "ace/Log_Msg.h"
#include "ace/OS.h"
#include "ace/Thread_Mutex.h"
#include <WinSock2.h>
#include <string.h>
#include <stdio.h>
#include "nids.h"
#include "ffcs_logger.h"
#include "msgdefine.h"
#include "my_ringbuffbased_fifo.h"

#include <ace/Log_Msg.h>
#include <ace/OS.h>
#include <ace/Configuration.h>
#include <ace/Configuration_Import_Export.h>
#include <string>
class offlinecaptor:public  ACE_Task<ACE_MT_SYNCH> 
{
public:
	offlinecaptor(void);
	~offlinecaptor(void);
	static offlinecaptor* get_instance();
	static void destroy_instance();
	int init();
	int load_config(const char * _config_filename);
	int svc();
	inline int set_offline_file(std::string filename){
		_offline_file=filename;
		return 0;
	}
	inline int set_cached_ringfifo(my_fifo<tcp_connection_info>* fifo){
		if(!fifo){
			return -1;
		}
		_ringfifo=fifo;
		return 0;
	}
	inline int setLogger(Logger* logger){
		if(!logger){
			return -1;
		}
		_Log=logger;
		return 0;
	}
	inline Logger* getLogger(){
		return _Log;
	}
	inline void stopsvc(){

	}
	inline const std::string& get_err_msg()const{
		return _errmsg;
	}
private:
	friend void offline_tcp_callback(struct tcp_stream *tcp_http_connection, void **param);
	void put_data_into_fifo(tcp_connection_info& tci);
private:
	static  ACE_Thread_Mutex _mutex;
	static offlinecaptor* _instance;
	Logger*  _Log;
	my_fifo<tcp_connection_info>* _ringfifo;
	char _device[128];
	std::string _errmsg;
	std::string _offline_file;

};

