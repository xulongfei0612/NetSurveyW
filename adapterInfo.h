#ifndef _ADAPTER_INFO_H
#define _ADAPTER_INFO_H
#include <vector>
#include <string>
#include <winsock2.h> 
#include <iphlpapi.h> 
#include <Mprapi.h>
#pragma comment( lib, "Iphlpapi.lib")
#pragma comment( lib, "Mprapi.lib" )


struct MyAdapterInfo{
	char ipaddress[16];
	char lanFriendName[260];
	char AdapterName[260];
	char Description[132];
	
};

class adapterInfo
{
public:
	adapterInfo(void);
	~adapterInfo(void);
	std::vector<MyAdapterInfo>& get_all_adapters();
	std::string get_sel_adapter(int index);
private:
	std::vector<MyAdapterInfo> _allAdapters;
};

#endif