#include "Pcre.h"
#include <stdio.h>

Pcre::Pcre()
{
	error=0;
	re_arr.clear();
	patten_name.clear();
}

Pcre::~Pcre()
{
	for(int i=0; i<re_arr.size(); i++)
        {
                pcre_free(re_arr[i]);
        }
}

int Pcre::AddRule(const std::string &name, const std::string &patten)
{
	pcre *re = pcre_compile(patten.c_str(), PCRE_MULTILINE|PCRE_UTF8|PCRE_NO_AUTO_CAPTURE, (const char**)&error, &erroffset, NULL);
	if(re == NULL)
	{
		printf("pcre compile failed, offset %d: %s\n", erroffset, error);
		return -1;
	}
	else
	{
		re_arr.push_back(re);
		patten_name.push_back(name);
		return 0;
	}
}
void Pcre::ClearRules()
{
	for(int i=0; i<re_arr.size(); i++)
	{
		pcre_free(re_arr[i]); 
	}
	{
		std::vector<pcre*> tmp;
		re_arr.swap(tmp);
	}
	{
		std::vector<std::string> tmp;
		patten_name.swap(tmp);
	}
	//re_arr.clear();
	//patten_name.clear();
}

std::vector<MatchResult> Pcre::MatchAllRule(const char content[])
{
	std::vector<MatchResult> result_arr;
	int length = strlen(content);
	char buf[2048];
	for(int i=0; i<re_arr.size(); i++)
	{
		MatchResult result;
		result.name = patten_name[i];
		int cur = 0;
		int rc;
		while(cur<length && (rc = pcre_exec(re_arr[i], NULL, content, length, cur, PCRE_NOTEMPTY, ovector, VECSIZE)) >= 0)
		{
			for(int j=0; j<rc; j++)
			{
				memset(buf, 0, sizeof(buf));
				strncpy(buf, content+ovector[2*j], ovector[2*j+1]-ovector[2*j]);
				result.value.push_back(buf);
			}
			cur = ovector[1];
		}
		if(result.value.size() > 0)
			result_arr.push_back(result);
	}
	return result_arr;
}

