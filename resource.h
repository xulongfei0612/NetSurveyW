//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NetSurveyW.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_NETSURVEYW_DIALOG           102
#define IDD_DLG_REQBODY                 104
#define IDD_DLG_REQHEAD                 105
#define IDD_DLG_RESHEAD                 106
#define IDD_DLG_RESBODY                 107
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDD_DLG_CFG_REGEX               130
#define IDD_DLG_CFG_ADAPTER             131
#define IDR_MENU2                       132
#define IDC_TAB                         1001
#define IDC_TREE1                       1002
#define IDC_LIST_REQ_HEAD               1003
#define IDC_LIST_RES_HEAD               1004
#define IDC_EDIT_RES_BODY               1005
#define IDC_EDIT_REQ_BODY               1006
#define IDC_BUTTON_SS                   1007
#define IDC_LIST_REQ_RES                1008
#define IDC_BUTTON_DETAIL_ABSTRACT      1009
#define IDC_STATIC_CURR                 1010
#define IDC_STATIC_ALL                  1011
#define IDC_STATIC_MID                  1012
#define IDC_BUTTON_PRE                  1013
#define IDC_BUTTON_NEXT                 1014
#define IDC_BUTTON_CLEAR                1015
#define IDC_LIST_SHOW_RESULT            1016
#define IDC_BUTTON_CFG_REGEX            1017
#define IDC_EDIT_REGEX_NAME             1018
#define IDC_EDIT_REGEX_PATTERN          1019
#define IDC_BUTTON_ADD_PATTERN          1020
#define IDC_LIST_SHOW_INSERTED_REGEX    1021
#define IDC_BUTTON_SELECT_ALL           1022
#define IDC_BUTTON_SELECT_NONE          1023
#define IDC_BUTTON_DELETE               1024
#define IDC_LIST_SHOW_ADAPTER           1025
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_LISBOX_32773                 32773
#define ID_LISBOX_32774                 32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
