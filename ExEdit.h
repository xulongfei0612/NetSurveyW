#pragma once


// CExEdit

class CExEdit : public CEdit
{
	DECLARE_DYNAMIC(CExEdit)

public:
	CExEdit();
	virtual ~CExEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

	CMenu  _menu;
};


