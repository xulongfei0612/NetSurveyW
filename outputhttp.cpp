#include "outputhttp.h"


outputhttp::outputhttp(void):_outdir(""),_fout(0)
{
}


outputhttp::~outputhttp(void)
{
	destroy();
}
int outputhttp::create(){

	if(!FolderExists((LPCTSTR)parentdir_)){
		if(!CreateDirectory((LPCTSTR)parentdir_,NULL)){
			fprintf(stderr, "create datadir error:[%s]\n",parentdir_);
			return -1;
		}
	}
	_outdir.append(parentdir_);
	_outdir.append("/");
	time_t t;
	time(&t);
	struct tm* now=localtime(&t);
	char ftime[64]={0};
	
	sprintf(ftime,"%d年_%d月_%d日__%d时_%d分_%d秒",now->tm_year+1900,now->tm_mon+1,now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec);
    _outdir.append(ftime);
	_outdir.append(".log");
	_fout=fopen(_outdir.c_str(),"a+");
	if(_fout==0){
	    int err=GetLastError();
		return -1;
	}
	ACE_DEBUG((LM_DEBUG,ACE_TEXT("http log 根目录：[%s] \n"),parentdir_));
	return 0;
}

int outputhttp::output(struct http_interaction* hi){
	
	fprintf(_fout,"******************************************************\n");
	fprintf(_fout,"request...................:\n");
	fprintf(_fout,"url:%s \n",hi->request->url);
	fprintf(_fout,"method:%s \n",hi->request->method);
	fprintf(_fout,"host:%s \n",hi->request->host);
	fprintf(_fout,"sip:%s \n",hi->request->saddr);
	fprintf(_fout,"dip:%s \n",hi->request->daddr);
	fprintf(_fout,"sport:%d \n",hi->request->srcPort);
	fprintf(_fout,"dport:%d \n",hi->request->desPort);
	fprintf(_fout,"userAgent:%s \n",hi->request->userAgent);
	fprintf(_fout,"cookie:%s \n",hi->request->cookie);
	fprintf(_fout,"content:%s \n",hi->request->content);
	fprintf(_fout,"---------------------------------------------------------\n");
	fprintf(_fout,"response...................:\n");
	fprintf(_fout,"resCode:%s \n",hi->response->resCode);
	fprintf(_fout,"contentType:%s \n",hi->response->contentType);
	fprintf(_fout,"date:%s \n",hi->response->date);
	fprintf(_fout,"content:%s \n",hi->response->content);
	fprintf(_fout,"contentSize:%d \n",hi->response->content_size);
	fprintf(_fout,"#######################################################\n");
	return 0;
}
int outputhttp::output(struct http_session* hs){
	return 0;
}
int outputhttp::destroy(){
	if(_fout){
		fflush(_fout);
		fclose(_fout);
	}
		
	return 0;
}

BOOL outputhttp::FindFirstFileExists(LPCTSTR lpPath, DWORD dwFilter){
	WIN32_FIND_DATA fd;  
	HANDLE hFind = FindFirstFile(lpPath, &fd);  
	BOOL bFilter = (FALSE == dwFilter) ? TRUE : fd.dwFileAttributes & dwFilter;  
	BOOL RetValue = ((hFind != INVALID_HANDLE_VALUE) && bFilter) ? TRUE : FALSE;  
	FindClose(hFind);  
	return RetValue;  
}
BOOL outputhttp::FilePathExists(LPCTSTR lpPath){
	return FindFirstFileExists(lpPath, FALSE);  
}
BOOL outputhttp::FolderExists(LPCTSTR lpPath)  
{  
	return FindFirstFileExists(lpPath, FILE_ATTRIBUTE_DIRECTORY);  
} 

int outputhttp::load_config(const char * _config_filename){
	ACE_TString str;
	ACE_Configuration_Heap config;
	if (config.open() == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), ACE_TEXT("config.open()")), -1);
	}

	ACE_Ini_ImpExp config_importer(config);
	if (config_importer.import_config(ACE_TEXT(_config_filename)) == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), _config_filename), -1);
	}

	ACE_Configuration_Section_Key status_section;
	if (config.open_section (config.root_section(), ACE_TEXT(HTTP_LOG_CONF_SECTION), 0, status_section) == -1)
	{
		ACE_ERROR_RETURN ((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), ACE_TEXT ("Can't open [LOGGER] section")), -1);
	}

	/*日志输出根路径*/
	if (config.get_string_value(status_section, ACE_TEXT(HTTP_LOG_CONF_OUTDIR), str) != -1)
	{
		if (ACE_OS::strcmp(str.c_str(), this->parentdir_) != 0)
		{
			//ACE_DEBUG((LM_DEBUG, ACE_TEXT("(%P|%t) [%s]->'%s' is updated (%s)->(%s)/n"), HTTP_LOG_CONF_SECTION, HTTP_LOG_CONF_OUTDIR, this->parentdir_, str.c_str()));
			ACE_OS::memset(this->parentdir_, 0x00, sizeof(this->parentdir_));
			ACE_OS::strncpy(this->parentdir_, str.c_str(), sizeof(this->parentdir_));
		}
		str.clear();
	}
	return 0;
}