// RegexCfgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "RegexCfgDlg.h"
#include "afxdialogex.h"
#include "tinyxml.h"

// RegexCfgDlg 对话框

IMPLEMENT_DYNAMIC(RegexCfgDlg, CDialogEx)

RegexCfgDlg::RegexCfgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(RegexCfgDlg::IDD, pParent)
{

}

RegexCfgDlg::~RegexCfgDlg()
{
}

void RegexCfgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SHOW_INSERTED_REGEX, m_list);
	DDX_Control(pDX, IDC_EDIT_REGEX_NAME, m_editName);
	DDX_Control(pDX, IDC_EDIT_REGEX_PATTERN, m_editPattern);
}


BEGIN_MESSAGE_MAP(RegexCfgDlg, CDialogEx)
	
	ON_BN_CLICKED(IDC_BUTTON_ADD_PATTERN, &RegexCfgDlg::OnBnClickedButtonAddPattern)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_SELECT_ALL, &RegexCfgDlg::OnBnClickedButtonSelectAll)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_NONE, &RegexCfgDlg::OnBnClickedButtonSelectNone)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &RegexCfgDlg::OnBnClickedButtonDelete)
END_MESSAGE_MAP()


// RegexCfgDlg 消息处理程序








void RegexCfgDlg::OnBnClickedButtonAddPattern()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strName;
	CString strPattern;
	m_editName.GetWindowTextA(strName);
	m_editPattern.GetWindowTextA(strPattern);
	if(strName.IsEmpty()){
		MessageBox("正则名字不能为空");
		return ;
	}
	if(strPattern.IsEmpty()){
		MessageBox("正则表达式不能为空");
		return;
	}
	patterninfo  newpattern;
	newpattern.name=strName.GetBuffer();
	newpattern.pattern=strPattern.GetBuffer();
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	add_pattern(papp->_patterns,m_list,newpattern);
	m_editName.SetWindowTextA("");
	m_editPattern.SetWindowTextA("");
}


BOOL RegexCfgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	m_list.InsertColumn(0,"序号",LVCFMT_LEFT,80);
	m_list.InsertColumn(1,"name",LVCFMT_LEFT,120);
	m_list.InsertColumn(2,"pattern",LVCFMT_LEFT,460);
	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	int ret=read_config(PATTERNS_CONFIG_FILE,papp->_patterns);
	if(ret<0){
		MessageBox("加载正则配置失败");
		return FALSE;
	}
	show_existing_patterns(m_list,papp->_patterns);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

int RegexCfgDlg::read_config(const char* file,std::vector<patterninfo>& patterns){
	TiXmlDocument  doc;
	int ret=0;
	ret=doc.LoadFile(file);
	if(!ret){
		return -1;
	}
	{
		std::vector<patterninfo> temp;
		patterns.swap(temp);
	}
	TiXmlElement* root=doc.RootElement();
	TiXmlElement* patternelems=root->FirstChildElement();
	TiXmlElement* pattern=patternelems->FirstChildElement();
	for(;pattern;pattern=pattern->NextSiblingElement()){
		patterninfo newpattern;
		newpattern.name=pattern->Attribute("name");
		newpattern.pattern=pattern->Attribute("pattern");
		patterns.push_back(newpattern);
	}
	return 0;
}
int RegexCfgDlg::write_config(const char* file,std::vector<patterninfo>& patterns){
	TiXmlDocument *doc=new TiXmlDocument;
	TiXmlElement* root=new TiXmlElement("config");
	TiXmlElement* patternelems=new TiXmlElement("patterns");
	std::vector<patterninfo>::const_iterator coit=patterns.begin();
	for(;coit!=patterns.end();++coit){
		TiXmlElement* pattern=new TiXmlElement("regex");
		pattern->SetAttribute("name",coit->name.c_str());
		pattern->SetAttribute("pattern",coit->pattern.c_str());
		patternelems->LinkEndChild(pattern);
	}
	root->LinkEndChild(patternelems);
	doc->LinkEndChild(root);
	int ret=0;
	ret=doc->SaveFile(file);
	if(!ret){
		delete doc;
		return -1;
	}
	delete doc;
	return 0;
}

int RegexCfgDlg::show_existing_patterns(CListCtrl& listctrl,std::vector<patterninfo>& patterns){
	std::vector<patterninfo>::const_iterator coit=patterns.begin();
	int index=0;
	CString strNum;
	for (;coit!=patterns.end();++coit){
		strNum.Format("%d",index+1);
		listctrl.InsertItem(index,strNum);
		listctrl.SetItemText(index,1,coit->name.c_str());
		listctrl.SetItemText(index,2,coit->pattern.c_str());
		index++;
	}
	return 0;
}

int RegexCfgDlg::add_pattern(std::vector<patterninfo>& patterns,CListCtrl& listctrl,patterninfo& newpattern){
	patterns.push_back(newpattern);
	int index=listctrl.GetItemCount();
	CString strNum;
	strNum.Format("%d",index+1);
	listctrl.InsertItem(index,strNum);
	listctrl.SetItemText(index,1,newpattern.name.c_str());
	listctrl.SetItemText(index,2,newpattern.pattern.c_str());
	return 0;
}
int RegexCfgDlg::delete_pattern(int item,std::vector<patterninfo>& patterns,CListCtrl& listctrl){
	std::vector<patterninfo>::iterator it=patterns.begin();
	int index=0;
	for (;it!=patterns.end();)
	{
		if(index==item){
			it=patterns.erase(it);
			break;
		}
		else{
			++it;
		}
		index++;
	}
	listctrl.DeleteItem(item);
	return 0;
}
void RegexCfgDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	int ret=write_config(PATTERNS_CONFIG_FILE,papp->_patterns);
	if(ret<0){
		MessageBox("写入正则配置失败");
	}
}


void RegexCfgDlg::OnBnClickedButtonSelectAll()
{
	// TODO: 在此添加控件通知处理程序代码
	int item=m_list.GetItemCount();
	for (int i=0;i<item;i++)
	{
		m_list.SetCheck(i,1);
	}
}


void RegexCfgDlg::OnBnClickedButtonSelectNone()
{
	// TODO: 在此添加控件通知处理程序代码
	int item=m_list.GetItemCount();
	for (int i=0;i<item;i++)
	{
		m_list.SetCheck(i,0);
	}
}


void RegexCfgDlg::OnBnClickedButtonDelete()
{
	// TODO: 在此添加控件通知处理程序代码
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	int item=m_list.GetItemCount();
	for (int i=item-1;i>=0;i--)
	{
		if(m_list.GetCheck(i)==1){
			delete_pattern(i,papp->_patterns,m_list);
		}
	}
}
