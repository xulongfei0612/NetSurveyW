#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// httpResponseBody 对话框

class httpResponseBody : public CDialogEx
{
	DECLARE_DYNAMIC(httpResponseBody)

public:
	httpResponseBody(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~httpResponseBody();

// 对话框数据
	enum { IDD = IDD_DLG_RESBODY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog();

	CEdit m_edit;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
