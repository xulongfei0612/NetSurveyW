#pragma once
#include "afxcmn.h"
#include <string>
#include "adapterInfo.h"
// AdapterCfgDlg 对话框

class AdapterCfgDlg : public CDialogEx
{
	DECLARE_DYNAMIC(AdapterCfgDlg)

public:
	AdapterCfgDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AdapterCfgDlg();

// 对话框数据
	enum { IDD = IDD_DLG_CFG_ADAPTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_list;
	adapterInfo  adp;
	int _cursel;
	int write_config(const char * _config_filename,std::string& value);
	virtual void OnOK();
	afx_msg void OnLvnItemchangedListShowAdapter(NMHDR *pNMHDR, LRESULT *pResult);
};
