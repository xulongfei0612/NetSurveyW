#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "ExEdit.h"

// httpRequestBody 对话框
#define  USER_REQUEST_BODY_REGEX_MSG   WM_USER+4
class httpRequestBody : public CDialogEx
{
	DECLARE_DYNAMIC(httpRequestBody)

public:
	httpRequestBody(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~httpRequestBody();

// 对话框数据
	enum { IDD = IDD_DLG_REQBODY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	inline void setparentHwnd(HWND hwnd){
		_hwnd=hwnd;
	}
	CString getEditSelectedText(CEdit& edit); 
	CExEdit m_edit;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void On32772();
	HWND _hwnd;
};
