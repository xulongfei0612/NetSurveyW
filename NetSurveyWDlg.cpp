
// NetSurveyWDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "NetSurveyWDlg.h"
#include "afxdialogex.h"
#include "tinyxml.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
HWND  ghwnd;
void session_callback(http_session* phs,int tcpstate){
	WPARAM  wp=(WPARAM)phs;
	LPARAM  lp=(LPARAM)tcpstate;
	//SendMessage(ghwnd,USER_MSG_TEST,wp,lp);    //不能用postmessage 传递指针 因为post 放到消息队列 如果处理不及时 很可能指针已被改变
}

void interaction_callback(http_interaction* phi,int interstate){
	WPARAM  wp=(WPARAM)phi;
	LPARAM  lp=(LPARAM)interstate;
	SendMessage(ghwnd,USER_INTERACTION_MSG,wp,lp);  
}

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CNetSurveyWDlg 对话框




CNetSurveyWDlg::CNetSurveyWDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CNetSurveyWDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_survey=0;
	_inserted_num=0;
	_select_step=0;
	_sub_inserted_num=0;
	_online_cap_running=0;

}

CNetSurveyWDlg::~CNetSurveyWDlg(){

	for(int i=0;i<4;i++){
		if(m_dlg[i]){
			m_dlg[i]->DestroyWindow();
		}
	}	
	if(_survey){
		delete _survey;
	}
	destroy_interaction_vec(_interaction_vec);
	ACE_Object_Manager::instance()->fini();

}

void CNetSurveyWDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB, m_tab);
	DDX_Control(pDX, IDC_TREE1, m_tree);
	DDX_Control(pDX, IDC_BUTTON_SS, m_butss);
	DDX_Control(pDX, IDC_LIST_REQ_RES, m_abstractList);
	DDX_Control(pDX, IDC_BUTTON_DETAIL_ABSTRACT, m_butda);
	DDX_Control(pDX, IDC_STATIC_CURR, m_currstep);
	DDX_Control(pDX, IDC_STATIC_ALL, m_allstep);
	DDX_Control(pDX, IDC_STATIC_MID, m_sepcha);
	DDX_Control(pDX, IDC_BUTTON_PRE, m_prestep);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_nextstep);
	DDX_Control(pDX, IDC_BUTTON_CLEAR, m_butclear);
	DDX_Control(pDX, IDC_LIST_SHOW_RESULT, m_listShowRegexResult);
	DDX_Control(pDX, IDC_BUTTON_CFG_REGEX, m_butcfgRegex);
}

BEGIN_MESSAGE_MAP(CNetSurveyWDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, &CNetSurveyWDlg::OnTcnSelchangeTab)
	ON_BN_CLICKED(IDC_BUTTON_SS, &CNetSurveyWDlg::OnBnClickedButtonSs)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, &CNetSurveyWDlg::OnTvnSelchangedTree1)
	ON_WM_CLOSE()
	ON_MESSAGE(USER_MSG_TEST,onSessionMsg)
	ON_MESSAGE(USER_INTERACTION_MSG,onInteractionMsg)
	ON_MESSAGE(USER_REQUEST_HEAD_REGEX_MSG,onHandleReqheadMsg)
	ON_MESSAGE(USER_REQUEST_BODY_REGEX_MSG,onHandleReqboyMsg)
	ON_BN_CLICKED(IDC_BUTTON_DETAIL_ABSTRACT, &CNetSurveyWDlg::OnBnClickedButtonDetailAbstract)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_REQ_RES, &CNetSurveyWDlg::OnNMDblclkListReqRes)
	ON_BN_CLICKED(IDC_BUTTON_PRE, &CNetSurveyWDlg::OnBnClickedButtonPre)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CNetSurveyWDlg::OnBnClickedButtonNext)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_REQ_RES, &CNetSurveyWDlg::OnLvnItemchangedListReqRes)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CNetSurveyWDlg::OnBnClickedButtonClear)
	ON_BN_CLICKED(IDC_BUTTON_CFG_REGEX, &CNetSurveyWDlg::OnBnClickedButtonCfgRegex)
	ON_COMMAND(ID_LISBOX_32773, &CNetSurveyWDlg::OnLisbox32773)
	ON_COMMAND(ID_LISBOX_32774, &CNetSurveyWDlg::OnLisbox32774)
	ON_COMMAND(ID_32775, &CNetSurveyWDlg::OnOpenFile)
	ON_COMMAND(ID_32776, &CNetSurveyWDlg::OnQuit)
	ON_COMMAND(ID_32777, &CNetSurveyWDlg::OnOpenDumpFile)
	ON_COMMAND(ID_32778, &CNetSurveyWDlg::OnQuitPrs)
	ON_COMMAND(ID_32779, &CNetSurveyWDlg::OnSaveAs)
END_MESSAGE_MAP()


// CNetSurveyWDlg 消息处理程序

BOOL CNetSurveyWDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_tab.MoveWindow(DETAIL_TAB_X,DETAIL_TAB_Y,DETAIL_TAB_WIDTH,DETAIL_TAB_HIGHT);
	m_tree.MoveWindow(TRAFFIC_TREE_X,TRAFFIC_TREE_Y,TRAFFIC_TREE_WIDTH,TRAFFIC_TREE_HIGHT);
	m_abstractList.MoveWindow(ABSTRACT_LIST_X,ABSTRACT_LIST_Y,ABSTRACT_LIST_WIDTH,ABSTRACT_LIST_HIGHT);
	m_butss.MoveWindow(START_STOP_BUTTON_X,START_STOP_BUTTON_Y,START_STOP_BUTTON_WIDTH,START_STOP_BUTTON_HIGHT);
	m_butda.MoveWindow(DETAIL_ABSTRACT_BUTTON_X,DETAIL_ABSTRACT_BUTTON_Y,DETAIL_ABSTRACT_BUTTON_WIDTH,DETAIL_ABSTRACT_BUTTON_HIGHT);


	m_prestep.MoveWindow(PRE_STEP_BUTTON_X,PRE_STEP_BUTTON_Y,PRE_STEP_BUTTON_WIDTH,PRE_STEP_BUTTON_HIGHT);
	m_currstep.MoveWindow(STATIC_CURR_X,STATIC_CURR_Y,STATIC_CURR_WIDTH,STATIC_CURR_HIGHT);
	m_sepcha.MoveWindow(STATIC_MID_X,STATIC_MID_Y,STATIC_MID_WIDTH,STATIC_MID_HIGHT);
	m_allstep.MoveWindow(STATIC_ALL_X,STATIC_ALL_Y,STATIC_ALL_WIDTH,STATIC_ALL_HIGHT);
	m_nextstep.MoveWindow(NEXT_STEP_BUTTON_X,NEXT_STEP_BUTTON_Y,NEXT_STEP_BUTTON_WIDTH,NEXT_STEP_BUTTON_HIGHT);
	m_butclear.MoveWindow(CLEAR_BUTTON_X,CLEAR_BUTTON_Y,CLEAR_BUTTON_WIDTH,CLEAR_BUTTON_HIGHT);
	m_butcfgRegex.MoveWindow(CFG_REGEX_BUTTON_X,CFG_REGEX_BUTTON_Y,CFG_REGEX_BUTTON_WIDTH,CFG_REGEX_BUTTON_HIGHT);
	m_listShowRegexResult.MoveWindow(SHOW_REGEX_RESULT_LISTBOX_X,SHOW_REGEX_RESULT_LISTBOX_Y,SHOW_REGEX_RESULT_LISTBOX_WIDTH,SHOW_REGEX_RESULT_LISTBOX_HIGHT);
	m_prestep.ShowWindow(SW_HIDE);
	m_currstep.ShowWindow(SW_HIDE);
	m_sepcha.ShowWindow(SW_HIDE);
	m_allstep.ShowWindow(SW_HIDE);
	m_nextstep.ShowWindow(SW_HIDE);
	m_abstractList.InsertColumn(0,"URL",LVCFMT_LEFT,500);
	m_abstractList.InsertColumn(1,"方法",LVCFMT_LEFT,50);
	m_abstractList.InsertColumn(2,"结果",LVCFMT_LEFT,100);
	m_abstractList.InsertColumn(3,"类型",LVCFMT_LEFT,200);
	//m_abstractList.InsertColumn(4,"已接收",LVCFMT_LEFT,200);
	m_abstractList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	//int n = m_abstractList.GetItemCount();
	//m_abstractList.InsertItem(n,"http://www.baidu.com");
	//
	//m_abstractList.SetItemText(n,1,"GET");
	//m_abstractList.SetItemText(n,2,"200");
	//m_abstractList.SetItemText(n,3,"text");
	//m_abstractList.SetItemText(n,4,"7K");
	//n = m_abstractList.GetItemCount();
	//m_abstractList.InsertItem(n,"http://www.google.com");
	//m_abstractList.SetItemText(n,1,"POST");
	//m_abstractList.SetItemText(n,2,"200");
	//m_abstractList.SetItemText(n,3,"text");
	//m_abstractList.SetItemText(n,4,"1K");


	m_tab.ShowWindow(SW_HIDE);
	m_tab.InsertItem(0,"请求标头");
	m_tab.InsertItem(1,"请求正文");
	m_tab.InsertItem(2,"响应标头");
	m_tab.InsertItem(3,"响应正文");
	CRect rc;
	m_tab.GetClientRect(rc);
	rc.top += 22;
	rc.bottom -= 0;
	rc.left += 0;
	rc.right -= 0;



	_reqbody.Create(IDD_DLG_REQBODY,&m_tab);
	_reqhead.Create(IDD_DLG_REQHEAD,&m_tab);
	_resbody.Create(IDD_DLG_RESBODY,&m_tab);
	_reshead.Create(IDD_DLG_RESHEAD,&m_tab);

	_reqbody.MoveWindow(&rc);
	_reqhead.MoveWindow(&rc);
	_resbody.MoveWindow(&rc);
	_reshead.MoveWindow(&rc);

	_reqhead.setparentHwnd(this->m_hWnd);
	_reqbody.setparentHwnd(this->m_hWnd);
	m_dlg[0]=&_reqhead;
	m_dlg[1]=&_reqbody;
	m_dlg[2]=&_reshead;
	m_dlg[3]=&_resbody;

	m_dlg[0]->ShowWindow(SW_SHOW);
	m_dlg[1]->ShowWindow(SW_HIDE);
	m_dlg[2]->ShowWindow(SW_HIDE);
	m_dlg[3]->ShowWindow(SW_HIDE);
	_curr_sel=0;

	int level=0;
	hTreeRoot=m_tree.InsertItem("All Traffic",NULL,NULL);
	m_tree.SetItemData(hTreeRoot,(UINT_PTR)level);
	m_tree.Expand(hTreeRoot,TVE_EXPAND);
	ghwnd=this->m_hWnd;
	ACE_Object_Manager::instance()->init();
	srand((int)time(0));
	int ret=0;
	_survey=new survey;
	ret=_survey->create();
	if(ret<0){
		MessageBox("survey create error");
		return FALSE;
	}
	_survey->set_dumpfilepath(get_dumpfilepath().GetBuffer());
	_survey->register_callback(session_callback);
	_survey->register_interaction_callback(interaction_callback);
	m_butda.EnableWindow(FALSE);
	m_listShowRegexResult.AddString("显示正则匹配结果:");
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	read_config(PATTERNS_CONFIG_FILE,papp->_patterns);

	_menu.LoadMenuA(IDR_MENU2);
	SetMenu(&_menu);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CNetSurveyWDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CNetSurveyWDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CNetSurveyWDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CNetSurveyWDlg::OnTcnSelchangeTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	m_dlg[_curr_sel]->ShowWindow(SW_HIDE);
	//得到新的页面索引
	_curr_sel = m_tab.GetCurSel();
	//把新的页面显示出来
	m_dlg[_curr_sel]->ShowWindow(SW_SHOW);
	*pResult = 0;
}


void CNetSurveyWDlg::OnBnClickedButtonSs()
{
	// TODO: 在此添加控件通知处理程序代码
	CString  strText;
	m_butss.GetWindowTextA(strText);
	if(strText.Compare("开始")==0){
		//
		m_butss.SetWindowTextA("停止");
		start();
	}
	else{
		m_butss.SetWindowTextA("开始");
		stop();

	}
}


void CNetSurveyWDlg::OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	//if(_survey->is_running()){
	//	_survey->stop();
	//	m_butss.SetWindowTextA("开始");
	//}
	//_lock.Lock();
	get_selected_tree_interaction(m_tree);
	show_selected_tree_interaction(m_abstractList,_sub_interaction_vec);
	switch_to_abstract_view();
	_sub_inserted_num=_sub_interaction_vec.size();
	if(_sub_inserted_num==0){
		m_butda.EnableWindow(FALSE);
	}
	else{
		m_butda.EnableWindow(TRUE);
	}
	_select_step=0;
	CString strallstep;
	strallstep.Format("%d",_sub_inserted_num);
	m_allstep.SetWindowTextA(strallstep);
	//_lock.Unlock();
	*pResult = 0;

}
void CNetSurveyWDlg::switch_to_abstract_view(){
	m_abstractList.ShowWindow(SW_SHOW);
	m_tab.ShowWindow(SW_HIDE);

	m_prestep.ShowWindow(SW_HIDE);
	m_currstep.ShowWindow(SW_HIDE);
	m_sepcha.ShowWindow(SW_HIDE);
	m_allstep.ShowWindow(SW_HIDE);
	m_nextstep.ShowWindow(SW_HIDE);
	m_butda.SetWindowTextA("转到详细视图");

}
void CNetSurveyWDlg::switch_to_detail_view(){
	m_abstractList.ShowWindow(SW_HIDE);
	m_tab.ShowWindow(SW_SHOW);

	m_prestep.ShowWindow(SW_SHOW);
	m_currstep.ShowWindow(SW_SHOW);
	m_sepcha.ShowWindow(SW_SHOW);
	m_allstep.ShowWindow(SW_SHOW);
	m_nextstep.ShowWindow(SW_SHOW);
}

void CNetSurveyWDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(MessageBox("是否真的退出？","NetSurvey",MB_YESNO)==IDNO){
		return ;
	}
	_survey->stop();
	CDialogEx::OnClose();
}


void CNetSurveyWDlg::OnCancel()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialogEx::OnCancel();
}

void CNetSurveyWDlg::start()
{
	CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)threadrun,this,0,NULL);
	_online_cap_running=1;
}
void CNetSurveyWDlg::open_dump_file(const char* filename){
	this->_dumpfile=filename;
	CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)threadrun_offline,this,0,NULL);
}
void CNetSurveyWDlg::close_dump_file(){
	_survey->stop_offline();
}
void CNetSurveyWDlg::stop(){
	_survey->stop();
	_online_cap_running=0;
}
void CNetSurveyWDlg::clear(){
	_survey->reset();
	clear_insert_session_index_list(_inserted_interaction_list);
	clear_insert_session_index_list(_sub_inserted_interaction_list);
	clear_interaction_vec(_sub_interaction_vec);
	destroy_interaction_vec(_interaction_vec);
	_inserted_num=0;
	_sub_inserted_num=0;
	_select_step=0;
	m_abstractList.DeleteAllItems();
	clear_child_tree_item(m_tree,hTreeRoot);
	m_butss.SetWindowTextA("开始");
	switch_to_abstract_view();
	m_butda.EnableWindow(FALSE);
	_online_cap_running=0;
}
void func(){

	PostMessage(ghwnd,USER_MSG_TEST,0,0);
}
unsigned int CNetSurveyWDlg::threadFunc(void* para){
	while(1){
		func();
		Sleep(200);
	}
	return 0;
}

LRESULT CNetSurveyWDlg::onSessionMsg(WPARAM wparam,LPARAM lparam){
    http_session* hs=(http_session*)wparam;
	int tcpstate=(int)lparam;
	CString strSip;
	CString strSport;
	CString strDip;
	CString strDport;
	strSip.Format("%s",inet_ntoa(*((struct in_addr*) &(hs->sip))));
	strDip.Format("%s",inet_ntoa(*((struct in_addr*) &(hs->dip))));
	strSport.Format("%d",hs->sport);
	strDport.Format("%d",hs->dport);
	//strSip=randomSip();
	//strSport=randomSport();
	//strDip=randomDip();
	if(tcpstate==tcp_conn_just_est){
		strDip.Insert(0,"<---->");
		strSport.Append("<---->80");
		int level=0;
		HTREEITEM  hSipItem=/*findTreeItem(hTreeRoot,strSip)*/loopfindTreeItem(hTreeRoot,strSip);
		if(hSipItem){
			//update data
			HTREEITEM  hDipItem=/*findTreeItem(hSipItem,strDip)*/loopfindTreeItem(hSipItem,strDip);
			if(hDipItem){
				HTREEITEM  hportItem=/*findTreeItem(hDipItem,strSport)*/loopfindTreeItem(hDipItem,strSport);
				if(hportItem){

				}
				else{
					level=3;
					HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hDipItem);
					m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
				}
			}
			else{
				level=2;
				HTREEITEM hnewDipItem=m_tree.InsertItem(strDip,0,0,hSipItem);
				m_tree.SetItemData(hnewDipItem,(DWORD_PTR)level);
				level=3;
				HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hnewDipItem);
				m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
			}
		}
		else{
			level=1;
			HTREEITEM hnewSipItem=m_tree.InsertItem(strSip,0,0,hTreeRoot);
			m_tree.SetItemData(hnewSipItem,(DWORD_PTR)level);
			level=2;
			HTREEITEM hnewDipItem=m_tree.InsertItem(strDip,0,0,hnewSipItem);
			m_tree.SetItemData(hnewDipItem,(DWORD_PTR)level);
			level=3;
			HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hnewDipItem);
			m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
			m_tree.Invalidate(1);
			m_tree.Expand(hTreeRoot,TVE_EXPAND);
			m_tree.Expand(hnewSipItem,TVE_EXPAND);
		}
	}
	else if(tcpstate==tcp_conn_data){	
		//Ntuple4 key;
		//key.saddr=hs->sip;
		//key.source=hs->sport;
		//key.daddr=hs->dip;
		//key.dest=hs->dport;
		//inserted_session_info  isi;
		//std::map<Ntuple4,inserted_session_info>::const_iterator coit=inserted_sessions.find(key);
		//if(coit!=inserted_sessions.end()){
		//	std::vector<struct http_interaction*>::reverse_iterator interit=hs->interactions.rbegin();
		//	if(interit!=hs->interactions.rend()){
		//		CString strres;
		//		CString strtype;
		//		strres.Format("%s",(*interit)->response->resCode);
		//		strtype.Format("%s",(*interit)->response->contentType);
		//		int index=hs->interactions.size()+coit->second.interacSize-1;
		//		if(strres.IsEmpty()){
		//			return 1;
		//		}
		//		else{
		//			m_abstractList.SetItemText(index,2,strres);
		//		}
		//		if(strtype.IsEmpty()){
		//			return 1;
		//		}
		//		else{
		//			m_abstractList.SetItemText(index,3,strtype);
		//		}
		//		
		//	}
		//}
		//else{
		//	bool first=0;
		//	int sessionfirstindex=0;
		//	std::vector<struct http_interaction*>::iterator interit=hs->interactions.begin();
		//	for(;interit!=hs->interactions.end();++interit){
		//		CString strurl;
		//		CString strmethod;
		//		//CString strStartTime;
		//		strurl.Format("%s",(*interit)->request->url);
		//		strmethod.Format("%s",(*interit)->request->method);
		//		//strStartTime.Format("%d",hs->start_time);
		//		int n = m_abstractList.GetItemCount();
		//		if(first==0){
		//			sessionfirstindex=n;
		//		}
		//		m_abstractList.InsertItem(n,strurl);
		//		m_abstractList.SetItemText(n,1,strmethod);
		//		m_abstractList.SetItemText(n,4,"7K");
		//		++_inserted_num;
		//		CString strinserted;
		//		strinserted.Format("%d",_inserted_num);
		//		m_allstep.SetWindowTextA(strinserted);
		//		first++;
		//	}
		//	isi.startRow=sessionfirstindex;
		//	isi.interacSize=hs->interactions.size();
		//	inserted_sessions.insert(make_pair(key,isi));
		//}

	}
	else if(tcpstate==tcp_conn_close){
		//Ntuple4 key;
		//key.saddr=hs->sip;
		//key.source=hs->sport;
		//key.daddr=hs->dip;
		//key.dest=hs->dport;
		//std::map<Ntuple4,inserted_session_info>::iterator it=inserted_sessions.find(key);
		//if(it!=inserted_sessions.end()){
		//	inserted_sessions.erase(it);
		//}

	}
	//if(_inserted_num>0){
	//	m_butda.EnableWindow(TRUE);
	//}
	return 0;
}
HTREEITEM CNetSurveyWDlg::findTreeItem(HTREEITEM curr,CString strtext){
	HTREEITEM hfind;  
	if(curr == NULL)   
		return NULL;   
	while(curr!=NULL)   
	{  
		if(m_tree.GetItemText(curr) == strtext)   
			return curr;   
		if(m_tree.ItemHasChildren(curr))   
		{   
			curr = m_tree.GetChildItem(curr);  
			hfind = findTreeItem(curr,strtext);   
			if(hfind)   
			{   
				return hfind;   
			}   
			else 
				curr = m_tree.GetNextSiblingItem(m_tree.GetParentItem(curr));   
		}   
		else{ 
			curr = m_tree.GetNextSiblingItem(curr);   
		}   
	}   
	return curr;   
}
unsigned int CNetSurveyWDlg::threadrun(void* para){
	CNetSurveyWDlg* pthis=(CNetSurveyWDlg*)para;
	pthis->_survey->run();
	return 0;
}
unsigned int CNetSurveyWDlg::threadrun_offline(void* para){
	CNetSurveyWDlg* pthis=(CNetSurveyWDlg*)para;
	pthis->_survey->run_offline(pthis->_dumpfile);
	int i=0;
	return 0;
}
unsigned int CNetSurveyWDlg::threadprocess(void* para){
	CNetSurveyWDlg* pthis=(CNetSurveyWDlg*)para;
	while(1){
		//pthis->_lock.Lock();
		//pop 
		//pthis->_lock.Unlock();
		//process

		Sleep(10);
	}
	return 0;
}

CString CNetSurveyWDlg::randomSip(){
	CString   strSip[10]={"192.168.10.1","192.168.10.2","192.168.10.3","192.168.10.4","192.168.10.5",
	"192.168.10.6","192.168.10.7","192.168.10.8","192.168.10.9","192.168.10.10"};
	int i=rand()%10;
	return strSip[i];
}
CString CNetSurveyWDlg::randomSport(){
	CString strSport[10]={"12345","12344","12348","14554","56562","12324","16565","11466","16767","23435"};
	return strSport[rand()%10];
}

CString CNetSurveyWDlg::randomDip(){
	CString   strSip[10]={"192.168.10.100","192.168.10.99","192.168.10.98","192.168.10.97","192.168.10.96",
		"192.168.10.95","192.168.10.94","192.168.10.93","192.168.10.92","192.168.10.19"};
	int i=rand()%10;
	return strSip[i];
}

void CNetSurveyWDlg::OnFinalRelease()
{
	// TODO: 在此添加专用代码和/或调用基类

	CDialogEx::OnFinalRelease();
}


void CNetSurveyWDlg::OnBnClickedButtonDetailAbstract()
{
	// TODO: 在此添加控件通知处理程序代码
	CString  strText;
	m_butda.GetWindowTextA(strText);
	if(strText.Compare("转到详细视图")==0){
		//
		m_butda.SetWindowTextA("返回摘要视图");
		m_abstractList.ShowWindow(SW_HIDE);
		m_tab.ShowWindow(SW_SHOW);	
		m_prestep.ShowWindow(SW_SHOW);
		m_currstep.ShowWindow(SW_SHOW);
		m_sepcha.ShowWindow(SW_SHOW);
		m_allstep.ShowWindow(SW_SHOW);
		m_nextstep.ShowWindow(SW_SHOW);
		CString strCurrStep;
		strCurrStep.Format("%d",_select_step+1);
		m_currstep.SetWindowTextA(strCurrStep);
		//_lock.Lock();
		insertReqHead(_sub_interaction_vec[_select_step]->request);
		insertReqBody(_sub_interaction_vec[_select_step]->request);
		insertResHead(_sub_interaction_vec[_select_step]->response);
		insertResBody(_sub_interaction_vec[_select_step]->response);
		//_lock.Unlock();
	}
	else{
		m_butda.SetWindowTextA("转到详细视图");
		m_tab.ShowWindow(SW_HIDE);
		m_abstractList.ShowWindow(SW_SHOW);
		m_prestep.ShowWindow(SW_HIDE);
		m_currstep.ShowWindow(SW_HIDE);
		m_sepcha.ShowWindow(SW_HIDE);
		m_allstep.ShowWindow(SW_HIDE);
		m_nextstep.ShowWindow(SW_HIDE);
		//CString strInserted;
		//strInserted.Format("%d",_inserted_num);
		//MessageBox(strInserted);
	}

}





void CNetSurveyWDlg::OnNMDblclkListReqRes(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	if(_sub_inserted_num==0){
		return ;
	}
	int nRow=pNMItemActivate->iItem;
	if(nRow<0){
		return ;
	}
	m_butda.SetWindowTextA("返回摘要视图");
	m_abstractList.ShowWindow(SW_HIDE);
	m_tab.ShowWindow(SW_SHOW);
	m_prestep.ShowWindow(SW_SHOW);
	m_currstep.ShowWindow(SW_SHOW);
	m_sepcha.ShowWindow(SW_SHOW);
	m_allstep.ShowWindow(SW_SHOW);
	m_nextstep.ShowWindow(SW_SHOW);
	CString strCurrStep;
	strCurrStep.Format("%d",_select_step+1);
	m_currstep.SetWindowTextA(strCurrStep);
	//_lock.Lock();
	insertReqHead(_sub_interaction_vec[nRow]->request);
	insertReqBody(_sub_interaction_vec[nRow]->request);
	insertResHead(_sub_interaction_vec[nRow]->response);
	insertResBody(_sub_interaction_vec[nRow]->response);
	//_lock.Unlock();
	*pResult = 0;
}


HTREEITEM  CNetSurveyWDlg::loopfindTreeItem(HTREEITEM curr,CString strtext){
	if(curr==NULL)
		return NULL;
	//std::stack<HTREEITEM> itemstack;
	//HTREEITEM finded;
	//itemstack.push(curr);
	//while(!itemstack.empty()){
	//	finded=itemstack.top();
	//	itemstack.pop();
	//	if(m_tree.GetItemText(finded)==strtext){
	//		return finded;
	//	}
	//	else{
	//		if(m_tree.ItemHasChildren(finded)){
	//			HTREEITEM child=m_tree.GetChildItem(finded);
	//			itemstack.push(child);
	//			for (;child;child=m_tree.GetNextSiblingItem(child))
	//			{
	//				itemstack.push(child);
	//			}
	//		}
	//	}
	//}
	std::list<HTREEITEM> itemlist;
	HTREEITEM finded;
	itemlist.push_back(curr);
	while(!itemlist.empty()){
		finded=itemlist.front();
		itemlist.pop_front();
		if(m_tree.GetItemText(finded)==strtext){
			return finded;
		}
		else{
			if(m_tree.ItemHasChildren(finded)){
				HTREEITEM child=m_tree.GetChildItem(finded);
				itemlist.push_back(child);
				for (;child;child=m_tree.GetNextSiblingItem(child))
				{
					itemlist.push_back(child);
				}
			}
		}
	}
	return NULL;
}

void CNetSurveyWDlg::OnBnClickedButtonPre()
{
	// TODO: 在此添加控件通知处理程序代码
	//m_prestep.SetWindowTextA();
	CString strCurrStep;
	m_currstep.GetWindowTextA(strCurrStep);
	CString strAllStep;
	m_allstep.GetWindowTextA(strAllStep);

	int curr;
	curr=atoi(strCurrStep);
	int all;
	all=atoi(strAllStep);
	curr-=2;
	if(curr==-1){
		curr=all-1;
	}
	//_lock.Lock();
	insertReqHead(_sub_interaction_vec[curr]->request);
	insertReqBody(_sub_interaction_vec[curr]->request);
	insertResHead(_sub_interaction_vec[curr]->response);
	insertResBody(_sub_interaction_vec[curr]->response);
	//_lock.Unlock();
	strCurrStep.Format("%d",curr+1);
	m_currstep.SetWindowTextA(strCurrStep);
}


void CNetSurveyWDlg::OnBnClickedButtonNext()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strCurrStep;
	m_currstep.GetWindowTextA(strCurrStep);
	CString strAllStep;
	m_allstep.GetWindowTextA(strAllStep);

	int curr;
	curr=atoi(strCurrStep);
	int all;
	all=atoi(strAllStep);
	--curr;
	curr=(curr+1)%all;
	//_lock.Lock();
	int n=_interaction_vec.size();
	insertReqHead(_sub_interaction_vec[curr]->request);
	insertReqBody(_sub_interaction_vec[curr]->request);
	insertResHead(_sub_interaction_vec[curr]->response);
	insertResBody(_sub_interaction_vec[curr]->response);
	//_lock.Unlock();
	strCurrStep.Format("%d",curr+1);
	m_currstep.SetWindowTextA(strCurrStep);
}





void CNetSurveyWDlg::OnLvnItemchangedListReqRes(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	_select_step=pNMLV->iItem;
	*pResult = 0;
}

LRESULT CNetSurveyWDlg::onInteractionMsg(WPARAM wparam,LPARAM lparam){
	http_interaction* hi=(http_interaction*)wparam;
	int interstate=(int)lparam;
	if(interstate==interaction_new_request){
		if(_inserted_num>=MAX_CAP_NUM){
			CString  strMax;
			strMax.Format("捕获条目达到上限%d，请清除数据，重新开始",MAX_CAP_NUM);
			MessageBox(strMax);
			_survey->stop();
			return 1;
		}
		Ntuple4 n4;
		n4.saddr=hi->request->sip;
		n4.daddr=hi->request->dip;
		n4.source=hi->request->srcPort;
		n4.dest=hi->request->desPort;
		insert_new_traffic(m_tree,n4);
		//CString strurl;
		//CString strmethod;

		//strurl.Format("%s",hi->request->url);
		//strmethod.Format("%s",hi->request->method);

		//int n = m_abstractList.GetItemCount();
		//m_abstractList.InsertItem(n,strurl);
		//m_abstractList.SetItemText(n,1,strmethod);
		//m_abstractList.SetItemText(n,4,"7K");
	

		inserted_session_info one_insert_session;
		one_insert_session.startRow=_inserted_num;
		one_insert_session.hi=hi;
		_inserted_interaction_list.push_back(one_insert_session);
		++_inserted_num;
		//CString strallstep;
		//strallstep.Format("%d",_inserted_num);
		//m_allstep.SetWindowTextA(strallstep);
		http_interaction* newhi=0;
		create_interaction(&newhi);
		httprequest* newreq=0;
		create_request(&newreq);
		httpresponse* newres=0;
		create_response(&newres);
		newhi->request=newreq;
		newhi->response=newres;
		memcpy(newhi->request,hi->request,sizeof(httprequest));
		memcpy(newhi->response,hi->response,sizeof(httpresponse));
		//_lock.Lock();
		_interaction_vec.push_back(newhi);
		//_sub_interaction_vec.push_back(newhi);
		add_interaction_to_selected_item(_sub_interaction_vec,newhi,m_tree,m_abstractList);
		//_lock.Unlock();
	}
	else if(interstate==interaction_new_response){
		CString strres;
		CString strtype;
		strres.Format("%s",hi->response->resCode);
		strtype.Format("%s",hi->response->contentType);
		int insert_index=get_interaction_insertIndex_from_listCtrl(_inserted_interaction_list,hi);
		if(insert_index!=-1){
			//if(!strres.IsEmpty()){
			//	m_abstractList.SetItemText(insert_index,2,strres);
			//}
			//if(!strtype.IsEmpty()){
			//	m_abstractList.SetItemText(insert_index,3,strtype);
			//}
			//_lock.Lock();
			memcpy(_interaction_vec[insert_index]->response,hi->response,sizeof(httpresponse));
			update_selected_item_interaction(_interaction_vec[insert_index],m_tree,m_abstractList);
			//_lock.Unlock();
		}
		//_lock.Lock();
		
		//_lock.Unlock();
	}
	else if(interstate==interaction_continued_response){
		int insert_index=get_interaction_insertIndex_from_listCtrl(_inserted_interaction_list,hi);
		if(insert_index!=-1){
			//_lock.Lock();
			memcpy(_interaction_vec[insert_index]->response,hi->response,sizeof(httpresponse));
			//_lock.Unlock();
		}
	}
	return 1;
}
std::vector<MatchResult> CNetSurveyWDlg::run_regex(const char* src,std::vector<patterninfo>& patterns){
	_survey->clear_rule();
	std::vector<patterninfo>::const_iterator coit=patterns.begin();
	for (;coit!=patterns.end();++coit)
	{
		_survey->add_rule(coit->name,coit->pattern);
	}
	return _survey->run_regex(src);
}
void CNetSurveyWDlg::show_regex_result(CListBox& listbox,std::vector<MatchResult>& result){
	CString strResult;
	if(result.size()==0){
		//listbox.AddString("没有匹配的项！");
		listbox.InsertString(-1,"没有匹配的项！");
		listbox.SetTopIndex(listbox.GetCount()-1);
		return ;
	}
	std::vector<MatchResult>::const_iterator co_result_it=result.begin();
	std::vector<std::string>::const_iterator co_sub_res_it;
	for (;co_result_it!=result.end();++co_result_it)
	{
		for(co_sub_res_it=co_result_it->value.begin();co_sub_res_it!=co_result_it->value.end();++co_sub_res_it){
			strResult.Format("匹配成功:  正则名字:%s   匹配结果：%s",co_result_it->name.c_str(),
				co_sub_res_it->c_str());
			//listbox.AddString(strResult);
			listbox.InsertString(-1,strResult);
			listbox.SetTopIndex(listbox.GetCount()-1);
		}
		
	}
	
}
LRESULT CNetSurveyWDlg::onHandleReqheadMsg(WPARAM wparam,LPARAM lparam){
	CString str=*((CString*)wparam);
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	std::vector<MatchResult> res=run_regex(str.GetBuffer(),papp->_patterns);
	show_regex_result(m_listShowRegexResult,res);
	return -1;
}
LRESULT CNetSurveyWDlg::onHandleReqboyMsg(WPARAM wparam,LPARAM lparam){
	CString str=*((CString*)wparam);
	CNetSurveyWApp* papp=(CNetSurveyWApp*)AfxGetApp();
	std::vector<MatchResult> res=run_regex(str.GetBuffer(),papp->_patterns);
	show_regex_result(m_listShowRegexResult,res);
	return -1;
}
void CNetSurveyWDlg::insertReqHead(httprequest* req){
	if(req){
		int n=0;
		_reqhead.m_list.DeleteAllItems();
		_reqhead.m_list.InsertItem(n,"请求");
		_reqhead.m_list.SetItemText(n,1,req->method);
		n++;
		_reqhead.m_list.InsertItem(n,"URL");
		_reqhead.m_list.SetItemText(n,1,req->url);
		n++;
		_reqhead.m_list.InsertItem(n,"Accept");
		_reqhead.m_list.SetItemText(n,1,req->accept);
		n++;
		_reqhead.m_list.InsertItem(n,"User-Agent");
		_reqhead.m_list.SetItemText(n,1,req->userAgent);
		n++;
		_reqhead.m_list.InsertItem(n,"Host");
		_reqhead.m_list.SetItemText(n,1,req->host);
		n++;
		_reqhead.m_list.InsertItem(n,"Cookie");
		_reqhead.m_list.SetItemText(n,1,req->cookie);
		n++;
	}
}
void CNetSurveyWDlg::insertReqBody(httprequest* req){
	if(req){
		_reqbody.m_edit.Clear();
		CString  strContent(req->content);
		if(strContent.IsEmpty()){
			_reqbody.m_edit.SetWindowTextA("没有要查看的数据");
			return ;
		}
		_reqbody.m_edit.SetWindowTextA(req->content);
	}
}
void CNetSurveyWDlg::insertResHead(httpresponse* res){
	if(res){
		_reshead.m_list.DeleteAllItems();
		int n=0;
		_reshead.m_list.InsertItem(n,"响应");
		_reshead.m_list.SetItemText(n,1,res->resCode);
		n++;
		_reshead.m_list.InsertItem(n,"Date");
		_reshead.m_list.SetItemText(n,1,res->date);
		n++;
		_reshead.m_list.InsertItem(n,"Content-Type");
		_reshead.m_list.SetItemText(n,1,res->contentType);
		n++;
		CString strCntSize;
		strCntSize.Format("%d",res->content_size);
		_reshead.m_list.InsertItem(n,"Content-Length");
		_reshead.m_list.SetItemText(n,1,strCntSize);
		n++;
	}
}
void CNetSurveyWDlg::insertResBody(httpresponse* res){
	if(res){
		_resbody.m_edit.Clear();
		CString  strContent(res->content);
		if(strContent.IsEmpty()){
			_resbody.m_edit.SetWindowTextA("没有要查看的数据");
			return ;
		}
		_resbody.m_edit.SetWindowTextA(res->content);
	}
}

void CNetSurveyWDlg::OnBnClickedButtonClear()
{
	// TODO: 在此添加控件通知处理程序代码
	clear();
}

void CNetSurveyWDlg::clear_interaction_vec(std::vector<http_interaction*>& vec){
	std::vector<http_interaction*> temp;
	vec.swap(temp);
}
void CNetSurveyWDlg::destroy_interaction_vec(std::vector<http_interaction*>& vec){
	std::vector<http_interaction*>::iterator it=vec.begin();
	for(;it!=vec.end();){
		//if(it->)
		destroy_request((*it)->request);
		destroy_response((*it)->response);
		destroy_interaction(*it);
		it=vec.erase(it);
	}
}


int CNetSurveyWDlg::create_interaction(http_interaction** hi){
	if(!hi){
		return -1;
	}
	try{
		if(*hi==0)
			*hi=new http_interaction;
		memset(*hi,0,sizeof(http_interaction));
	}
	catch(...){
		exit(0);
	}
	return 0;
}
int CNetSurveyWDlg::create_request(httprequest** req){
	if(!req){
		return -1;
	}
	try{
		if(*req==0)
			*req=new httprequest;
		memset(*req,0,sizeof(httprequest));
	}
	catch(...){
		exit(-1);
	}
	return 0;
}
int CNetSurveyWDlg::create_response(httpresponse** rep){
	if(!rep){
		return -1;
	}
	try{
		if(*rep==0)
			*rep=new httpresponse;
		memset(*rep,0,sizeof(httpresponse));
	}
	catch(...){
		exit(-1);
	}
	return 0;
}
int CNetSurveyWDlg::destroy_interaction(http_interaction* hi){
	if(!hi){
		return -1;
	}
	delete hi;
	hi=0;
	return 0;
}
int CNetSurveyWDlg::destroy_request(httprequest* req){
	if(req){
		delete req;
		req=0;
	}
	return 0;
}
int CNetSurveyWDlg::destroy_response(httpresponse* rep){
	if(rep){
		delete rep;
		rep=0;
	}
	return 0;
}
void CNetSurveyWDlg::show_selected_tree_interaction(CListCtrl& listctrl,std::vector<http_interaction*>& vec){
	CString strurl;
	CString strmethod;
	CString strres;
	CString strtype;
	int index=0;
	listctrl.DeleteAllItems();
	Sleep(100);
	std::vector<http_interaction*>::const_iterator coit=vec.begin();
	for(;coit!=vec.end();++coit){
		strurl.Format("%s",(*coit)->request->url);
		strmethod.Format("%s",(*coit)->request->method);
		strres.Format("%s",(*coit)->response->resCode);
		strtype.Format("%s",(*coit)->response->contentType);
		index=listctrl.GetItemCount();
		listctrl.InsertItem(index,strurl);
		listctrl.SetItemText(index,1,strmethod);
		listctrl.SetItemText(index,2,strres);
		listctrl.SetItemText(index,3,strtype);
	}
}
void CNetSurveyWDlg::get_selected_tree_interaction(CTreeCtrl& tree){
	int level=0;
	HTREEITEM ht=tree.GetSelectedItem();
	level=(int)tree.GetItemData(ht);
	switch(level){
	case 0:
		copy_all();
		break;
	case 1:
		copy_change_start_with_sip(m_tree,ht);
		break;
	case 2:
		copy_change_start_with_dip(m_tree,ht);
		break;
	case 3:
		copy_change_start_with_sportdport(m_tree,ht);
		break;
	default:
		break;
	}
}

void CNetSurveyWDlg::copy_all(){
	clear_interaction_vec(_sub_interaction_vec);
	std::vector<http_interaction*>::iterator sitbegin=_interaction_vec.begin();
	std::vector<http_interaction*>::iterator sitend=_interaction_vec.end();
	copy(sitbegin,sitend,back_insert_iterator<std::vector<http_interaction*> >(_sub_interaction_vec));
}
void CNetSurveyWDlg::copy_change_start_with_sip(CTreeCtrl& tree,HTREEITEM& curr){
	clear_interaction_vec(_sub_interaction_vec);
	CString  strSip=tree.GetItemText(curr);
	uint32_t sip=inet_addr(strSip.GetBuffer());
	if(sip==INADDR_NONE){
		return ;
	}
	std::vector<http_interaction*>::iterator sit=_interaction_vec.begin();
	for(;sit!=_interaction_vec.end();++sit){
		if((*sit)->request->sip==sip){
			_sub_interaction_vec.push_back(*sit);
		}
	}

}
void CNetSurveyWDlg::copy_change_start_with_dip(CTreeCtrl& tree,HTREEITEM& curr){
	clear_interaction_vec(_sub_interaction_vec);
	HTREEITEM sipItem=tree.GetParentItem(curr);
	if(!sipItem){
		return ;
	}
	CString  strSip=tree.GetItemText(sipItem);
	CString  strDip=tree.GetItemText(curr);
	strDip=strDip.Right(strDip.GetLength() - 6);
	uint32_t sip=inet_addr(strSip.GetBuffer());
	uint32_t dip=inet_addr(strDip.GetBuffer());
	if(sip==INADDR_NONE||dip==INADDR_NONE){
		return ;
	}
	std::vector<http_interaction*>::iterator sit=_interaction_vec.begin();
	for(;sit!=_interaction_vec.end();++sit){
		if((*sit)->request->sip==sip&&(*sit)->request->dip==dip){
			_sub_interaction_vec.push_back(*sit);
		}
	}
}
void CNetSurveyWDlg::copy_change_start_with_sportdport(CTreeCtrl& tree,HTREEITEM& curr){
	clear_interaction_vec(_sub_interaction_vec);

	HTREEITEM dipItem=tree.GetParentItem(curr);
	if(!dipItem){
		return ;
	}
	HTREEITEM sipItem=tree.GetParentItem(dipItem);
	if(!sipItem){
		return ;
	}
	CString  strSip=tree.GetItemText(sipItem);
	CString  strDip=tree.GetItemText(dipItem);
	CString  strSportDport=tree.GetItemText(curr);
	strDip=strDip.Right(strDip.GetLength() - 6);
	uint32_t sip=inet_addr(strSip.GetBuffer());
	uint32_t dip=inet_addr(strDip.GetBuffer());
	uint16_t sport=0;
	uint16_t dport=0;
	get_sport_dport_from_treeItemText(strSportDport,sport,dport);
	if(sip==INADDR_NONE||dip==INADDR_NONE){
		return ;
	}
	std::vector<http_interaction*>::iterator sit=_interaction_vec.begin();
	for(;sit!=_interaction_vec.end();++sit){
		if((*sit)->request->sip==sip&&(*sit)->request->dip==dip
			&&(*sit)->request->srcPort==sport&&(*sit)->request->desPort==dport){
			_sub_interaction_vec.push_back(*sit);
		}
	}
}

void CNetSurveyWDlg::clear_child_tree_item(CTreeCtrl& tree,HTREEITEM& root){
	std::list<HTREEITEM> itemlist;
	std::list<HTREEITEM> templist;
	HTREEITEM finded;
	itemlist.push_back(root);
	while(!itemlist.empty()){
		finded=itemlist.front();
		templist.push_back(finded);
		itemlist.pop_front();
		if(m_tree.ItemHasChildren(finded)){
			HTREEITEM child=m_tree.GetChildItem(finded);
			itemlist.push_back(child);
			for (;child;child=m_tree.GetNextSiblingItem(child))
			{
				itemlist.push_back(child);
			}
		}		
	}
	templist.pop_front();
	std::list<HTREEITEM>::iterator it=templist.begin();
	for(;it!=templist.end();++it){
		tree.DeleteItem(*it);
	}
	tree.SetRedraw(1);
}



void CNetSurveyWDlg::OnBnClickedButtonCfgRegex()
{
	// TODO: 在此添加控件通知处理程序代码
	RegexCfgDlg  dlg;
	if(dlg.DoModal()==IDOK){
		//MessageBox("OK");
	}else{
		//MessageBox("Not OK");
	}
	
}


void CNetSurveyWDlg::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类

}


BOOL CNetSurveyWDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN){
		if(pMsg->wParam==VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
int CNetSurveyWDlg::read_config(const char* file,std::vector<patterninfo>& patterns){
	TiXmlDocument  doc;
	int ret=0;
	ret=doc.LoadFile(file);
	if(!ret){
		return -1;
	}
	{
		std::vector<patterninfo> temp;
		patterns.swap(temp);
	}
	TiXmlElement* root=doc.RootElement();
	TiXmlElement* patternelems=root->FirstChildElement();
	TiXmlElement* pattern=patternelems->FirstChildElement();
	for(;pattern;pattern=pattern->NextSiblingElement()){
		patterninfo newpattern;
		newpattern.name=pattern->Attribute("name");
		newpattern.pattern=pattern->Attribute("pattern");
		patterns.push_back(newpattern);
	}
	return 0;
}
CString CNetSurveyWDlg::get_listbox_selected_text(CExListBox& listbox){
	int selcount=listbox.GetSelCount();
	if(selcount==-1){
		return "";
	}
	int* selindex=new int[selcount];
	CString strText;
	if(selindex){
		listbox.GetSelItems(selcount,selindex);
		for(int i=0;i<selcount;i++){
			CString strTemp;
			listbox.GetText(selindex[i],strTemp);
			strText+=strTemp;
			strText+="\t\n";
		}
		delete[]selindex;
	}
	return strText;
}
int CNetSurveyWDlg::copy_content_to_clipboard(CString& strText){
	if (OpenClipboard())
	{
		EmptyClipboard();
		HGLOBAL hClipboardData;
		hClipboardData = GlobalAlloc(GMEM_DDESHARE,strText.GetLength()+1);
		char * pchData;
		pchData = (char*)GlobalLock(hClipboardData);
		strcpy(pchData, LPCSTR(strText));
		GlobalUnlock(hClipboardData);
		SetClipboardData(CF_TEXT,hClipboardData);
		CloseClipboard();
		GlobalFree(hClipboardData);
		return 0;
	}
	return -1;
}
int CNetSurveyWDlg::clear_all_content(CExListBox& listbox){
	int index=listbox.GetCount();
	for (int i=index-1;i>=0;i--)
	{
		listbox.DeleteString(i);
	}
	return 0;
}
void CNetSurveyWDlg::OnLisbox32773()
{
	// TODO: 在此添加命令处理程序代码
	CString strText=get_listbox_selected_text(m_listShowRegexResult);
	copy_content_to_clipboard(strText);

}


void CNetSurveyWDlg::OnLisbox32774()
{
	// TODO: 在此添加命令处理程序代码
	clear_all_content(m_listShowRegexResult);
}
void CNetSurveyWDlg::add_interaction_to_selected_item(std::vector<http_interaction*>& selected,http_interaction* pint
	,CTreeCtrl& treectrl,CListCtrl& listctrl){
	if(!pint){
		return;
	}
	HTREEITEM selitem=treectrl.GetSelectedItem();
	if(selitem){
		CString strText=treectrl.GetItemText(selitem);
		int level=treectrl.GetItemData(selitem);
		switch(level){
		case 0:
			{
				selected.push_back(pint);
				add_interaction_to_listctrl(pint,listctrl);
				_sub_inserted_num=selected.size();
				CString strallstep;
				strallstep.Format("%d",_sub_inserted_num);
				m_allstep.SetWindowTextA(strallstep);
			}

			break;
		case 1:    //sip start
			{
				uint32_t sip=inet_addr(strText.GetBuffer());
				if(pint->request->sip==sip){
					selected.push_back(pint);
					add_interaction_to_listctrl(pint,listctrl);
					_sub_inserted_num=selected.size();
					CString strallstep;
					strallstep.Format("%d",_sub_inserted_num);
					m_allstep.SetWindowTextA(strallstep);
				}
			}
			break;
		case 2:   //dip start
			{
				strText=strText.Right(strText.GetLength() - 6);
				uint32_t dip=inet_addr(strText.GetBuffer());
				HTREEITEM hsip=treectrl.GetParentItem(selitem);
				CString strsip=treectrl.GetItemText(hsip);
				uint32_t sip=inet_addr(strsip.GetBuffer());
				if(pint->request->sip==sip&&pint->request->dip==dip){
					selected.push_back(pint);
					add_interaction_to_listctrl(pint,listctrl);
					_sub_inserted_num=selected.size();
					CString strallstep;
					strallstep.Format("%d",_sub_inserted_num);
					m_allstep.SetWindowTextA(strallstep);
				}
			}
			break;
		case 3:  //sport start
			{
				uint16_t sport,dport;
				get_sport_dport_from_treeItemText(strText,sport,dport);
				HTREEITEM hdip=treectrl.GetParentItem(selitem);
				CString strdip=treectrl.GetItemText(hdip);
				strdip=strdip.Right(strdip.GetLength() - 6);
				uint32_t dip=inet_addr(strdip.GetBuffer());
				HTREEITEM hsip=treectrl.GetParentItem(hdip);
				CString strsip=treectrl.GetItemText(hsip);
				uint32_t sip=inet_addr(strsip.GetBuffer());
				if(pint->request->sip==sip&&pint->request->dip==dip
					&&pint->request->srcPort==sport&&pint->request->desPort==dport){
					selected.push_back(pint);
					add_interaction_to_listctrl(pint,listctrl);
					_sub_inserted_num=selected.size();
					CString strallstep;
					strallstep.Format("%d",_sub_inserted_num);
					m_allstep.SetWindowTextA(strallstep);
				}
			}
			break;
		default:
			break;
		}
	}
}
void CNetSurveyWDlg::update_selected_item_interaction(http_interaction* pint,CTreeCtrl& treectrl,
	CListCtrl& listctrl){
		HTREEITEM selitem=treectrl.GetSelectedItem();
		if(selitem){
			CString strText=treectrl.GetItemText(selitem);
			int level=treectrl.GetItemData(selitem);
			switch(level){
			case 0:
				{
					set_inserted_interaction_response_to_listctrl(pint,listctrl);
				}
				break;
			case 1:    //sip start
				{
					uint32_t sip=inet_addr(strText.GetBuffer());
					if(pint->request->sip==sip){
						set_inserted_interaction_response_to_listctrl(pint,listctrl);
					}
				}
				break;
			case 2:   //dip start
				{
					strText=strText.Right(strText.GetLength() - 6);
					uint32_t dip=inet_addr(strText.GetBuffer());
					HTREEITEM hsip=treectrl.GetParentItem(selitem);
					CString strsip=treectrl.GetItemText(hsip);
					uint32_t sip=inet_addr(strsip.GetBuffer());
					if(pint->request->sip==sip&&pint->request->dip==dip){
						set_inserted_interaction_response_to_listctrl(pint,listctrl);
					}
				}
				break;
			case 3:  //sport start
				{
					uint16_t sport,dport;
					get_sport_dport_from_treeItemText(strText,sport,dport);
					HTREEITEM hdip=treectrl.GetParentItem(selitem);
					CString strdip=treectrl.GetItemText(hdip);
					strdip=strdip.Right(strdip.GetLength() - 6);
					uint32_t dip=inet_addr(strdip.GetBuffer());
					HTREEITEM hsip=treectrl.GetParentItem(hdip);
					CString strsip=treectrl.GetItemText(hsip);
					uint32_t sip=inet_addr(strsip.GetBuffer());
					if(pint->request->sip==sip&&pint->request->dip==dip
						&&pint->request->srcPort==sport&&pint->request->desPort==dport){
							set_inserted_interaction_response_to_listctrl(pint,listctrl);
					}
				}
				break;
			default:
				break;
			}
		}
}
void CNetSurveyWDlg::set_inserted_interaction_response_to_listctrl(http_interaction* pint,CListCtrl& listctrl){
	CString strres;
	CString strtype;
	strres.Format("%s",pint->response->resCode);
	strtype.Format("%s",pint->response->contentType);
	int insert_index=get_interaction_insertIndex_from_listCtrl(_sub_inserted_interaction_list,pint);
	if(insert_index!=-1){
		if(!strres.IsEmpty()){
			listctrl.SetItemText(insert_index,2,strres);
		}
		if(!strtype.IsEmpty()){
			listctrl.SetItemText(insert_index,3,strtype);
		}
	}
}
void CNetSurveyWDlg::add_interaction_to_listctrl(http_interaction* pint,CListCtrl& listctrl){
	if(pint){
		CString strurl;
		CString strmethod;
		int index=0;
		strurl.Format("%s",pint->request->url);
		strmethod.Format("%s",pint->request->method);
		index=listctrl.GetItemCount();
		inserted_session_info  one_insert_session;
		one_insert_session.startRow=index;
		one_insert_session.hi=pint;
		_sub_inserted_interaction_list.push_back(one_insert_session);
		listctrl.InsertItem(index,strurl);
		listctrl.SetItemText(index,1,strmethod);
		//listctrl.SetItemText(index,4,"7K");
	
	}
}

void CNetSurveyWDlg::insert_new_traffic(CTreeCtrl& tree,Ntuple4& n4){
	CString strSip;
	CString strSport;
	CString strDip;
	CString strDport;
	strSip.Format("%s",inet_ntoa(*((struct in_addr*) &(n4.saddr))));
	strDip.Format("%s",inet_ntoa(*((struct in_addr*) &(n4.daddr))));
	strSport.Format("%d",n4.source);
	strDport.Format("%d",n4.dest);

	strDip.Insert(0,"<---->");
	strDport.Insert(0,"<---->");
	strSport.Append(strDport);
	int level=0;
	HTREEITEM  hSipItem=loopfindTreeItem(hTreeRoot,strSip);
	if(hSipItem){
		//update data
		HTREEITEM  hDipItem=loopfindTreeItem(hSipItem,strDip);
		if(hDipItem){
			HTREEITEM  hSportItem=loopfindTreeItem(hDipItem,strSport);
			if(hSportItem){

			}
			else{
				level=3;
				HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hDipItem);
				m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
			}
		}
		else{
			level=2;
			HTREEITEM hnewDipItem=m_tree.InsertItem(strDip,0,0,hSipItem);
			m_tree.SetItemData(hnewDipItem,(DWORD_PTR)level);
			level=3;
			HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hnewDipItem);
			m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
		}
	}
	else{
		level=1;
		HTREEITEM hnewSipItem=m_tree.InsertItem(strSip,0,0,hTreeRoot);
		m_tree.SetItemData(hnewSipItem,(DWORD_PTR)level);
		level=2;
		HTREEITEM hnewDipItem=m_tree.InsertItem(strDip,0,0,hnewSipItem);
		m_tree.SetItemData(hnewDipItem,(DWORD_PTR)level);
		level=3;
		HTREEITEM hnewSportItem=m_tree.InsertItem(strSport,0,0,hnewDipItem);
		m_tree.SetItemData(hnewSportItem,(DWORD_PTR)level);
		m_tree.Invalidate(1);
		m_tree.Expand(hTreeRoot,TVE_EXPAND);
		m_tree.Expand(hnewSipItem,TVE_EXPAND);
	}
	if(_sub_inserted_num>0){
		m_butda.EnableWindow(TRUE);
	}
}
int CNetSurveyWDlg::get_sport_dport_from_treeItemText(CString& strtext,uint16_t& sport,uint16_t& dport){
	if(strtext.IsEmpty()){
		return -1;
	}
	CString strSport;
	CString strDport;
	int nstart;
	nstart=strtext.Find("<");
	if(nstart<0){
		return -1;
	}
	strSport=strtext.Mid(0,nstart);
	nstart=strtext.Find(">");
	if(nstart<0){
		return -1;
	}
	strDport=strtext.Right(strtext.GetLength()-nstart-1);
	sport=atoi(strSport);
	dport=atoi(strDport);
	return 0;
}
int CNetSurveyWDlg::get_interaction_insertIndex_from_listCtrl(
	std::vector<struct inserted_session_info>& insertlist,http_interaction* hi){
		std::vector<struct inserted_session_info>::const_reverse_iterator crit=insertlist.rbegin();
		for(;crit!=insertlist.rend();++crit){
			if(crit->hi==hi){
				return crit->startRow;
			}
		}
		return -1;
}

int CNetSurveyWDlg::clear_insert_session_index_list(std::vector<struct inserted_session_info>& insertlist){
	std::vector<struct inserted_session_info>  temp;
	insertlist.swap(temp);
	insertlist.clear();
	return 0;
}

void CNetSurveyWDlg::OnOpenFile()
{
	// TODO: 在此添加命令处理程序代码
}


void CNetSurveyWDlg::OnQuit()
{
	// TODO: 在此添加命令处理程序代码
}


void CNetSurveyWDlg::OnOpenDumpFile()
{
	// TODO: 在此添加命令处理程序代码
	clear();
	char filter[]="PCAP File(*.pcap)|*.pcap|pcap File(*.*)|*.*||";
	CString strFilePath="";
	CFileDialog file(true, NULL,"*.pcap",OFN_HIDEREADONLY,filter,NULL );
	if(file.DoModal()==IDOK){
		strFilePath=file.GetPathName();
	}
	else{
		return ;
	}
	if(strFilePath.IsEmpty()){
		MessageBox("请选择合适的文件");
		return ;
	}
	close_dump_file();
	open_dump_file(strFilePath.GetBuffer());
}


void CNetSurveyWDlg::OnQuitPrs()
{
	// TODO: 在此添加命令处理程序代码
	PostQuitMessage(0);
}


void CNetSurveyWDlg::OnSaveAs()
{
	// TODO: 在此添加命令处理程序代码
	if(_online_cap_running){
		MessageBox("请先停止捕获");
		return ;
	}
	char filter[]="PCAP File(*.pcap)|*.pcap|pcap File(*.*)|*.*||";
	CString strFilePath="";
	CFileDialog file(false, "pcap",NULL,OFN_HIDEREADONLY,filter,NULL );
	if(file.DoModal()==IDOK){
		strFilePath=file.GetPathName();
	}
	else{
		return ;
	}
	if(strFilePath.IsEmpty()){
		return;
	}
	char buff[256];
	int rnum=0;
	char FilePath[MAX_PATH];
	CString strrfilepath;

	GetModuleFileName(0,FilePath,MAX_PATH-1);
	strrfilepath=FilePath;
	strrfilepath=strrfilepath.Left(strrfilepath.ReverseFind('\\'));
	strrfilepath=strrfilepath.Left(strrfilepath.ReverseFind('\\'));
	strrfilepath+="\\data\\NetSurvey.pcap";
	CFile  rfile(strrfilepath,CFile::modeRead);
	CFile  wfile(strFilePath,CFile::modeCreate|CFile::modeWrite);
	while ((rnum=rfile.Read(buff,sizeof(buff)))>0)
	{
		wfile.Write(buff,rnum);

	}
	wfile.Flush();
	wfile.Close();
	rfile.Close();
}
CString CNetSurveyWDlg::get_dumpfilepath(){
	char FilePath[MAX_PATH];
	CString strrfilepath;

	GetModuleFileName(0,FilePath,MAX_PATH-1);
	strrfilepath=FilePath;
	strrfilepath=strrfilepath.Left(strrfilepath.ReverseFind('\\'));
	strrfilepath=strrfilepath.Left(strrfilepath.ReverseFind('\\'));
	strrfilepath+="\\data\\NetSurvey.pcap";
	return strrfilepath;
}