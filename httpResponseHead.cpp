// httpResponseHead.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "httpResponseHead.h"
#include "afxdialogex.h"


// httpResponseHead 对话框

IMPLEMENT_DYNAMIC(httpResponseHead, CDialogEx)
int httpResponseHead::_count=0;
httpResponseHead::httpResponseHead(CWnd* pParent /*=NULL*/)
	: CDialogEx(httpResponseHead::IDD, pParent)
{

}

httpResponseHead::~httpResponseHead()
{

}

void httpResponseHead::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RES_HEAD, m_list);
}


BEGIN_MESSAGE_MAP(httpResponseHead, CDialogEx)
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// httpResponseHead 消息处理程序


BOOL httpResponseHead::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}





void httpResponseHead::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	// TODO: 在此处添加消息处理程序代码
	if(_count==1){
		CRect   rec;
		GetClientRect(&rec);
		m_list.MoveWindow(rec);
		m_list.InsertColumn(0,"键",LVCFMT_LEFT,rec.Width()/2);
		m_list.InsertColumn(1,"值",LVCFMT_LEFT,rec.Width()/2);
		m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	}
	_count++;
}


void httpResponseHead::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: 在此处添加消息处理程序代码
}
