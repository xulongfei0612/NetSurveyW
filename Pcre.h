#ifndef PCRE_H_
#define PCRE_H_
#include <pcre.h>
#include <string>
#include <vector>
#include <string.h>


const int VECSIZE = 30;

struct MatchResult
{
	std::string name;
	std::vector<std::string> value;
};

class Pcre
{
public:
	Pcre();
	~Pcre();

	int AddRule(const std::string &name, const std::string &patten);

	void ClearRules();

	std::vector<MatchResult> MatchAllRule(const char content[]);

private:
	char *error;
	int erroffset;
	int ovector[VECSIZE];
	std::vector<pcre*> re_arr;
	std::vector<std::string> patten_name;
};
#endif