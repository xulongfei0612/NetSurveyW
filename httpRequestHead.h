#pragma once
#include "afxcmn.h"

#define  USER_REQUEST_HEAD_REGEX_MSG   WM_USER+3
// httpRequestHead 对话框

class httpRequestHead : public CDialogEx
{
	DECLARE_DYNAMIC(httpRequestHead)

public:
	httpRequestHead(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~httpRequestHead();

// 对话框数据
	enum { IDD = IDD_DLG_REQHEAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	inline void setparentHwnd(HWND hwnd){
		_hwnd=hwnd;
	}
	CListCtrl m_list;
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	static int _count;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnNMRClickListReqHead(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void On32771();
	CString _strRegexSrc;
	HWND _hwnd;
};
