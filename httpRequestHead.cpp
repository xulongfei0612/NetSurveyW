// httpRequestHead.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "httpRequestHead.h"
#include "afxdialogex.h"


// httpRequestHead 对话框

IMPLEMENT_DYNAMIC(httpRequestHead, CDialogEx)
int httpRequestHead::_count=0;
httpRequestHead::httpRequestHead(CWnd* pParent /*=NULL*/)
	: CDialogEx(httpRequestHead::IDD, pParent)
{
	_hwnd=0;
}

httpRequestHead::~httpRequestHead()
{
}

void httpRequestHead::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_REQ_HEAD, m_list);
}


BEGIN_MESSAGE_MAP(httpRequestHead, CDialogEx)
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_RCLICK, IDC_LIST_REQ_HEAD, &httpRequestHead::OnNMRClickListReqHead)
	ON_COMMAND(ID_32771, &httpRequestHead::On32771)
END_MESSAGE_MAP()


// httpRequestHead 消息处理程序


BOOL httpRequestHead::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void httpRequestHead::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	// TODO: 在此处添加消息处理程序代码
	if(_count==1){
		CRect   rec;
		GetClientRect(&rec);
		m_list.MoveWindow(rec);
		m_list.InsertColumn(0,"键",LVCFMT_LEFT,rec.Width()/2);
		m_list.InsertColumn(1,"值",LVCFMT_LEFT,rec.Width()/2);
		m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	}
	_count++;
}


void httpRequestHead::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
	// TODO: 在此处添加消息处理程序代码
}


void httpRequestHead::OnNMRClickListReqHead(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	//MessageBox("R click");
	CMenu menu;
	menu.LoadMenu(IDR_MENU1);//IDR_MENU_Datagram 资源里菜单的ID
	CMenu *pMenu = menu.GetSubMenu(0);
	CPoint pt;
	GetCursorPos(&pt);	
	pMenu->TrackPopupMenu (TPM_LEFTALIGN,pt.x,pt.y,this);
	menu.DestroyMenu();
	_strRegexSrc=m_list.GetItemText(pNMItemActivate->iItem,1);
	*pResult = 0;
}


void httpRequestHead::On32771()
{
	// TODO: 在此添加命令处理程序代码
	//MessageBox(_strRegexSrc);
	if(_hwnd){
		::SendMessage(_hwnd,USER_REQUEST_HEAD_REGEX_MSG,(WPARAM)&_strRegexSrc,0);
	}
}
