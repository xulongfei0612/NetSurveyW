// ExEdit.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "ExEdit.h"


// CExEdit

IMPLEMENT_DYNAMIC(CExEdit, CEdit)

CExEdit::CExEdit()
{
	_menu.LoadMenu(IDR_MENU1);;
}

CExEdit::~CExEdit()
{
	_menu.DestroyMenu();
}


BEGIN_MESSAGE_MAP(CExEdit, CEdit)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()



// CExEdit 消息处理程序




void CExEdit::OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/)
{
	// TODO: 在此处添加消息处理程序代码

	//IDR_MENU_Datagram 资源里菜单的ID
	CMenu *pMenu = _menu.GetSubMenu(1);
	CPoint pt;
	GetCursorPos(&pt);	
	pMenu->TrackPopupMenu (TPM_LEFTALIGN,pt.x,pt.y,GetParent());
	
}
