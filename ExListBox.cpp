// ExListBox.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "ExListBox.h"


// CExListBox

IMPLEMENT_DYNAMIC(CExListBox, CListBox)

CExListBox::CExListBox()
{

}

CExListBox::~CExListBox()
{
}


BEGIN_MESSAGE_MAP(CExListBox, CListBox)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()



// CExListBox 消息处理程序




void CExListBox::OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/)
{
	// TODO: 在此处添加消息处理程序代码
	CMenu  menu;
	menu.LoadMenuA(IDR_MENU1);
	CMenu *pMenu = menu.GetSubMenu(2);
	CPoint pt;
	GetCursorPos(&pt);	
	pMenu->TrackPopupMenu (TPM_LEFTALIGN,pt.x,pt.y,GetParent());
	menu.DestroyMenu();
}
