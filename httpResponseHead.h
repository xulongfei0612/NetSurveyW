#pragma once
#include "afxcmn.h"


// httpResponseHead 对话框

class httpResponseHead : public CDialogEx
{
	DECLARE_DYNAMIC(httpResponseHead)

public:
	httpResponseHead(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~httpResponseHead();

// 对话框数据
	enum { IDD = IDD_DLG_RESHEAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_list;
	virtual BOOL OnInitDialog();

	afx_msg void OnSize(UINT nType, int cx, int cy);
	static int _count;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
