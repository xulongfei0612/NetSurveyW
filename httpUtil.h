/*
 * httpUtil.h
 *
 *  Created on: 2014��8��1��
 *      Author: Administrator
 */

#ifndef HTTPUTIL_H_
#define HTTPUTIL_H_
#include<iostream>
#include<string>
#include<stdint.h>
#include<stdlib.h>
#include<string.h>
#include <sstream>

//added by xlf  2014/8/1      parse http protocol from  one complete http session
class httpUtil {
public:
	httpUtil();
	virtual ~httpUtil();
	// begin parse
	int parse_request(const char* source,uint32_t len);
	int parse_response(const char* source,uint32_t len);




 inline	std::string& get_request_method(){
		return _request_method;
	 }
 inline	std::string& get_request_url(){
	    return _request_url;
 	 }
 inline	std::string& get_request_httpType(){
	 return _request_httpType;
 }
 inline	std::string& get_request_acceptLanguage(){
	 return _request_acceptLanguage;
 }
 inline	std::string& get_request_referer(){
	 return _request_referer;
 }
 inline	std::string& get_request_cookie(){
	 return _request_cookie;
 }
 inline	std::string& get_request_accept(){
	 return _request_accept;
 }
 inline	std::string& get_request_acceptEncoding(){
	 return _request_acceptEncoding;
 }
 inline	std::string& get_request_host(){
	 return _request_host;
 }
 inline std::string& get_request_connection(){
	 return _request_connection;
 }
 inline	std::string& get_request_payload(){
	 return _request_payload;
 }
 inline std::string& get_request_userAgent(){
	 return _request_userAgent;
 }

 inline	std::string& get_response_code(){
	 return _response_code;
 }
 inline	std::string& get_response_date(){
	 return _response_date;
 }
 inline	std::string& get_response_contentType(){
	 return _response_contentType;
 }
 inline	std::string& get_response_transferEncoding(){
	 return _response_transferEncoding;
 }
 inline	std::string& get_response_contentLength(){
	 return _response_contentLength;
 }
 inline	std::string& get_response_connection(){
	 return _response_connection;
 }
 inline	std::string& get_response_payload(){
	 return _response_payload;
 }
private:
 	 void clear_request();
     void clear_response();
     int get_header_field(const char* source,std::string& field,const char* fieldName);
     int dissect_request_firstLine();
     int dissect_response_firstLine();
     int dissect_response_chunked_transfer_entity(const char* source,uint32_t len);
 	std::string _request_firstLine;
	std::string _request_method;
	std::string _request_url;
	std::string _request_httpType;
	std::string _request_acceptLanguage;
	std::string _request_referer;
	std::string _request_cookie;
	std::string _request_accept;
	std::string _request_acceptEncoding;
	std::string _request_host;
	std::string _request_connection;
	std::string _request_payload;
	std::string _request_userAgent;

	std::string _response_firstLine;
	std::string _response_code;
	std::string _response_date;
	std::string _response_contentType;
	std::string _response_transferEncoding;
	std::string _response_contentLength;
	std::string _response_connection;
	std::string _response_payload;

};

#endif /* HTTPUTIL_H_ */
