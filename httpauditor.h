#pragma once
#include "ace/Task.h"  
#include <Windows.h>
#include "ace/Log_Msg.h"
#include "ace/OS.h"
#include "ffcs_logger.h"
#include "httpSession.h"
#include "httpUtil.h"

#include "msgdefine.h"
#include "Pcre.h"
#include "my_ringbuffbased_fifo.h"
class httpauditor:public ACE_Task<ACE_MT_SYNCH>       //auditor http data
{
public:
	httpauditor(void);
	~httpauditor(void);
	int create();
	int destroy();
	int load_config();
	int svc();
	int reset();

	void register_callback(SESSION_CALLBACK p);
	void register_interaction_callback(INTERACTION_CALLBACK p);
	inline int set_cached_ringfifo(my_fifo<tcp_connection_info>* fifo){
		if(!fifo){
			return -1;
		}
		_ringfifo=fifo;
		return 0;
	}
	inline int setLogger(Logger* logger){
		if(!logger){
			return -1;
		}
		_Log=logger;
		return 0;
	}
	inline Logger* getLogger(){
		return _Log;
	}
	inline void reset_quitflag(){
		_quitflag=0;
	}
	inline void set_quitflag(){
		_quitflag=1;
	}

	void testpcre(){
		Pcre  p;
		p.AddRule("test","\\w+");
		std::vector<MatchResult> res=p.MatchAllRule("xxxxsfwennf");
		if(res.size()>0){
			std::cout<<res[0].name<<"="<<res[0].value[0].c_str()<<std::endl;
		}
	}
	void teststring(){
		std::string str="123456789";
		char a[3]={0,'1','2'};
		str.append(a,3);
		std::cout <<str<<std::endl;
		std::cout<<str.length()<<std::endl;
	}
private:
	int process_data(tcp_connection_info* tci);
private:
	Logger* _Log;
	my_fifo<tcp_connection_info>* _ringfifo;
	httpSession* _httpsession;
	bool _quitflag;
};

