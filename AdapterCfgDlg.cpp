// AdapterCfgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "AdapterCfgDlg.h"
#include "afxdialogex.h"
#include <ace/Log_Msg.h>
#include <ace/OS.h>
#include <ace/Configuration.h>
#include <ace/Configuration_Import_Export.h>
#include <string>
#include "msgdefine.h"
// AdapterCfgDlg 对话框

IMPLEMENT_DYNAMIC(AdapterCfgDlg, CDialogEx)

AdapterCfgDlg::AdapterCfgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(AdapterCfgDlg::IDD, pParent)
{
	_cursel=0;
}

AdapterCfgDlg::~AdapterCfgDlg()
{
}

void AdapterCfgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SHOW_ADAPTER, m_list);
}


BEGIN_MESSAGE_MAP(AdapterCfgDlg, CDialogEx)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SHOW_ADAPTER, &AdapterCfgDlg::OnLvnItemchangedListShowAdapter)

END_MESSAGE_MAP()


// AdapterCfgDlg 消息处理程序


BOOL AdapterCfgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_list.InsertColumn(0,"Friendly Name",LVCFMT_LEFT,200);
	m_list.InsertColumn(1,"Desciption",LVCFMT_LEFT,250);
	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	
	std::vector<MyAdapterInfo>& res=adp.get_all_adapters();
	std::vector<MyAdapterInfo>::const_iterator coit=res.begin();
	for(;coit!=res.end();++coit){
		int n=m_list.GetItemCount();
		m_list.InsertItem(n,coit->lanFriendName);
		m_list.SetItemText(n,1,coit->Description);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


int AdapterCfgDlg::write_config(const char * _config_filename,std::string& value){
	ACE_TString str;
	ACE_Configuration_Heap config;
	str=value.c_str();
	if (config.open() == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), ACE_TEXT("config.open()")), -1);
	}

	ACE_Ini_ImpExp config_importer(config);
	if (config_importer.import_config(ACE_TEXT(_config_filename)) == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), _config_filename), -1);
	}

	ACE_Configuration_Section_Key status_section;
	if (config.open_section (config.root_section(), ACE_TEXT(CAP_CONF_SECTION), 0, status_section) == -1)
	{
		ACE_ERROR_RETURN ((LM_ERROR, ACE_TEXT("(%P|%t) %p\n"), ACE_TEXT ("Can't open [PCAPCONF] section")), -1);
	}

	if (config.set_string_value(status_section, ACE_TEXT(CAP_CONF_DEVICE), str) != -1)
	{
		//if (ACE_OS::strcmp(str.c_str(), _device) != 0)
		//{
		//	//ACE_DEBUG((LM_DEBUG, ACE_TEXT("(%P|%t) [%s]->'%s' is updated (%s)->(%s)/n"), HTTP_LOG_CONF_SECTION, HTTP_LOG_CONF_OUTDIR, this->parentdir_, str.c_str()));
		//	ACE_OS::memset(this->_device, 0x00, sizeof(this->_device));
		//	ACE_OS::strncpy(this->_device, str.c_str(), sizeof(this->_device));
		//}
		if(config_importer.export_config(_config_filename)!=-1){	
			str.clear();
			return 0;
		}
		
	}
	return -1;
}

void AdapterCfgDlg::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类
	if(_cursel==-1){
		MessageBox("请选择一个网卡");
		return ;
	}
	std::string value=adp.get_sel_adapter(_cursel);
	write_config(CAPTOR_CONFIG_FILE,value);
	CDialogEx::OnOK();
}


void AdapterCfgDlg::OnLvnItemchangedListShowAdapter(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	_cursel=pNMLV->iItem;
	*pResult = 0;
}


