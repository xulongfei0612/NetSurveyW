
// NetSurveyWDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "httpRequestBody.h"
#include "httpRequestHead.h"
#include "httpResponseBody.h"
#include "httpResponseHead.h"
#include "RegexCfgDlg.h"
#include "afxwin.h"

#include <map>
#include <stack>
#include <list>
#include <vector>
#include "survey.h"
#include "ExListBox.h"
using namespace std;
#define  USER_MSG_TEST   WM_USER+1
#define  USER_INTERACTION_MSG   WM_USER+2

  

#define TRAFFIC_TREE_X		0
#define TRAFFIC_TREE_Y		0
#define TRAFFIC_TREE_WIDTH	230	
#define TRAFFIC_TREE_HIGHT	570



#define DETAIL_TAB_X		230
#define DETAIL_TAB_Y		30
#define DETAIL_TAB_WIDTH	865
#define DETAIL_TAB_HIGHT	440

#define ABSTRACT_LIST_X		230
#define ABSTRACT_LIST_Y		30
#define ABSTRACT_LIST_WIDTH	865
#define ABSTRACT_LIST_HIGHT	440

#define START_STOP_BUTTON_X       250
#define START_STOP_BUTTON_Y			0
#define START_STOP_BUTTON_WIDTH		100
#define START_STOP_BUTTON_HIGHT		25

#define DETAIL_ABSTRACT_BUTTON_X       360
#define DETAIL_ABSTRACT_BUTTON_Y			0
#define DETAIL_ABSTRACT_BUTTON_WIDTH		100
#define DETAIL_ABSTRACT_BUTTON_HIGHT		25

#define PRE_STEP_BUTTON_X       480
#define PRE_STEP_BUTTON_Y			0
#define PRE_STEP_BUTTON_WIDTH		80
#define PRE_STEP_BUTTON_HIGHT		25

#define STATIC_CURR_X       575
#define STATIC_CURR_Y			5
#define STATIC_CURR_WIDTH		30
#define STATIC_CURR_HIGHT		20

#define STATIC_MID_X       610
#define STATIC_MID_Y			5
#define STATIC_MID_WIDTH		5
#define STATIC_MID_HIGHT		20

#define STATIC_ALL_X       621
#define STATIC_ALL_Y			5
#define STATIC_ALL_WIDTH		30
#define STATIC_ALL_HIGHT		20

#define NEXT_STEP_BUTTON_X       670
#define NEXT_STEP_BUTTON_Y			0
#define NEXT_STEP_BUTTON_WIDTH		80
#define NEXT_STEP_BUTTON_HIGHT		25

#define CLEAR_BUTTON_X       850
#define CLEAR_BUTTON_Y			0
#define CLEAR_BUTTON_WIDTH		100
#define CLEAR_BUTTON_HIGHT		25

#define CFG_REGEX_BUTTON_X      970
#define CFG_REGEX_BUTTON_Y			0
#define CFG_REGEX_BUTTON_WIDTH		100
#define CFG_REGEX_BUTTON_HIGHT		25

#define	SHOW_REGEX_RESULT_LISTBOX_X      230
#define SHOW_REGEX_RESULT_LISTBOX_Y			472
#define SHOW_REGEX_RESULT_LISTBOX_WIDTH		865
#define SHOW_REGEX_RESULT_LISTBOX_HIGHT		100

#define MAX_CAP_NUM                 5000
struct inserted_session_info{
	int startRow;
	http_interaction* hi;
};

// CNetSurveyWDlg 对话框
class CNetSurveyWDlg : public CDialogEx
{
// 构造
public:
	CNetSurveyWDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CNetSurveyWDlg();
// 对话框数据
	enum { IDD = IDD_NETSURVEYW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeTab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT onSessionMsg(WPARAM wparam,LPARAM lparam);
	afx_msg LRESULT onInteractionMsg(WPARAM wparam,LPARAM lparam);
	afx_msg LRESULT onHandleReqheadMsg(WPARAM wparam,LPARAM lparam);
	afx_msg LRESULT onHandleReqboyMsg(WPARAM wparam,LPARAM lparam);
public:
	void start();
	void stop();
	void clear();
	void open_dump_file(const char* filename);
	void close_dump_file();
private:
	static unsigned int __stdcall threadFunc(void* para);

	HTREEITEM  findTreeItem(HTREEITEM curr,CString strtext);
	static unsigned int __stdcall threadrun(void* para);
	static unsigned int __stdcall threadrun_offline(void* para);

	static unsigned int __stdcall threadprocess(void* para);

	HTREEITEM  loopfindTreeItem(HTREEITEM curr,CString strtext);

	void insertReqHead(httprequest* req);
	void insertReqBody(httprequest* req);
	void insertResHead(httpresponse* res);
	void insertResBody(httpresponse* res);

	int create_interaction(http_interaction** hi);
	int create_request(httprequest** req);
	int create_response(httpresponse** rep);
	int destroy_interaction(http_interaction* hi);
	int destroy_request(httprequest* req);
	int destroy_response(httpresponse* rep);

	void clear_interaction_vec(std::vector<http_interaction*>& vec);
	void destroy_interaction_vec(std::vector<http_interaction*>& vec);
	void insert_new_traffic(CTreeCtrl& tree,Ntuple4& n4);

	void get_selected_tree_interaction(CTreeCtrl& tree);
	void show_selected_tree_interaction(CListCtrl& listctrl,std::vector<http_interaction*>& vec);
	void copy_all();
	void copy_change_start_with_sip(CTreeCtrl& tree,HTREEITEM& curr);
	void copy_change_start_with_dip(CTreeCtrl& tree,HTREEITEM& curr);
	void copy_change_start_with_sportdport(CTreeCtrl& tree,HTREEITEM& curr);

	void add_interaction_to_selected_item(std::vector<http_interaction*>& selected,http_interaction* pint
		,CTreeCtrl& treectrl,CListCtrl& listctrl);
	void update_selected_item_interaction(http_interaction* pint,CTreeCtrl& treectrl,CListCtrl& listctrl);
	void add_interaction_to_listctrl(http_interaction* pint,CListCtrl& listctrl);
	void set_inserted_interaction_response_to_listctrl(http_interaction* pint,CListCtrl& listctrl);
	void switch_to_abstract_view();
	void switch_to_detail_view();
	void clear_child_tree_item(CTreeCtrl& tree,HTREEITEM& root);
	
	std::vector<MatchResult> run_regex(const char* src,std::vector<patterninfo>& patterns);
	void show_regex_result(CListBox& listbox,std::vector<MatchResult>& result);
	int read_config(const char* file,std::vector<patterninfo>& patterns);

	CString get_listbox_selected_text(CExListBox& listbox);
	int copy_content_to_clipboard(CString& strText);
	int clear_all_content(CExListBox& listbox);

	int get_sport_dport_from_treeItemText(CString& strtext,uint16_t& sport,uint16_t& dport); 
	int get_interaction_insertIndex_from_listCtrl(std::vector<struct inserted_session_info>& insertlist
		,http_interaction* hi);
	int clear_insert_session_index_list(std::vector<struct inserted_session_info>& insertlist);
	CString get_dumpfilepath();
private:

	CTabCtrl m_tab;
	httpRequestBody _reqbody;
	httpRequestHead _reqhead;
	httpResponseBody _resbody;
	httpResponseHead _reshead;
	CDialog* m_dlg[4];
	int _curr_sel;
	HANDLE  _hthread;
	HANDLE  _htreadpro;
	CTreeCtrl m_tree;
	CButton m_butss;
	HTREEITEM  hTreeRoot;
	CListCtrl m_abstractList;
	CButton m_butda;
	CStatic m_currstep;
	CStatic m_allstep;
	CStatic m_sepcha;
	CButton m_prestep;
	CButton m_nextstep;
	CButton m_butclear;
	//CCriticalSection _lock;

	survey* _survey;
	//std::map<Ntuple4,inserted_session_info> inserted_sessions;
	int _inserted_num;
	int _select_step;
	int _sub_inserted_num;
	std::vector<http_interaction*> _interaction_vec;
	std::vector<http_interaction*> _sub_interaction_vec;
	std::vector<struct inserted_session_info> _inserted_interaction_list;
	std::vector<struct inserted_session_info> _sub_inserted_interaction_list;

	CExListBox m_listShowRegexResult;
	CButton m_butcfgRegex;
	CMenu  _menu;
	CString _dumpfile;
	bool _online_cap_running;
public:
	
	CString randomSip();
	CString randomDip();
	CString randomSport();
	afx_msg void OnBnClickedButtonSs();
	
	afx_msg void OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnClose();
	virtual void OnCancel();
	virtual void OnFinalRelease();

	afx_msg void OnBnClickedButtonDetailAbstract();

	afx_msg void OnNMDblclkListReqRes(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonPre();
	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnLvnItemchangedListReqRes(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnBnClickedButtonClear();

	afx_msg void OnBnClickedButtonCfgRegex();
	virtual void OnOK();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLisbox32773();
	afx_msg void OnLisbox32774();
	afx_msg void OnOpenFile();
	afx_msg void OnQuit();
	afx_msg void OnOpenDumpFile();
	afx_msg void OnQuitPrs();
	afx_msg void OnSaveAs();
};
