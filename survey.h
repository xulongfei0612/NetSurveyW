#pragma once
#include <iostream>
#include <fstream>
#include "httpauditor.h"
#include "httpcaptor.h"
#include <ace/Thread_Manager.h>
#include "ace/Log_Msg.h"
#include "ace/Reactor.h"
#include <string.h>
#include "ffcs_logger.h"
#include "my_ringbuffbased_fifo.h"
#include "msgdefine.h"
#include "Pcre.h"
#include "offlinecaptor.h"

class survey
{
public:
	survey(void);
	~survey(void);
	int create();
	
	int load_config();

	int run();
	int stop();
	int reset();
	int run_offline(const char* file);
	int stop_offline();
	void register_callback(SESSION_CALLBACK p);
	void register_interaction_callback(INTERACTION_CALLBACK p);

	int add_rule(const std::string &name, const std::string &patten);
	void clear_rule();
	std::vector<MatchResult> run_regex(const char source[]);

	inline Logger& get_logger(){
		return _log;
	}
	inline bool is_running()const{
		return _run_flag;
	}
	inline bool set_dumpfilepath(const char* path){
		if(_captor){
			_captor->set_dumpfilepath(path);
			return true;
		}
		return false;
	}
private:
	int destroy();
	static void process_usr_quit(void* args);
private:
	Logger  _log;
	httpcaptor* _captor;
	httpauditor* _auditor;
	offlinecaptor* _off_captor;

	my_fifo<tcp_connection_info >* _ringfifo;
	Pcre  _pcre;
	int _run_flag;
};

