// httpRequestBody.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "httpRequestBody.h"
#include "afxdialogex.h"


// httpRequestBody 对话框

IMPLEMENT_DYNAMIC(httpRequestBody, CDialogEx)

httpRequestBody::httpRequestBody(CWnd* pParent /*=NULL*/)
	: CDialogEx(httpRequestBody::IDD, pParent)
{
	_hwnd=0;
}

httpRequestBody::~httpRequestBody()
{
}

void httpRequestBody::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_REQ_BODY, m_edit);
}


BEGIN_MESSAGE_MAP(httpRequestBody, CDialogEx)

	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_32772, &httpRequestBody::On32772)
END_MESSAGE_MAP()


// httpRequestBody 消息处理程序


BOOL httpRequestBody::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}





void httpRequestBody::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
	CRect   rec;
	GetClientRect(&rec);
	m_edit.MoveWindow(rec);
	// TODO: 在此处添加消息处理程序代码
}

CString httpRequestBody::getEditSelectedText(CEdit& edit){
	int start=0,end=0;
	edit.GetSel(start,end);
	if(start==end){
		return "";
	}
	CString strRet;
	edit.GetWindowText(strRet);
	DWORD dwNum = MultiByteToWideChar (CP_ACP, 0, strRet, -1, NULL, 0);
	wchar_t* pdes=new wchar_t[dwNum];
	if(!pdes){
		delete []pdes;
		return "";
	}
	MultiByteToWideChar(CP_ACP,0,strRet,-1,pdes,dwNum);
	CStringW strRetW(pdes);
	strRetW=strRetW.Mid(start,end-start);
	CString ret(strRetW.GetBuffer());
	delete []pdes;
	return ret;
}
void httpRequestBody::On32772()
{
	// TODO: 在此添加命令处理程序代码
	CString strText=getEditSelectedText(m_edit);
	//MessageBox(strText);
	if(_hwnd){
		::SendMessage(_hwnd,USER_REQUEST_BODY_REGEX_MSG,(WPARAM)&strText,0);
	}
}
