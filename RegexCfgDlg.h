#pragma once
#include "afxcmn.h"
#include "msgdefine.h"
#include "NetSurveyW.h"
#include "afxwin.h"
// RegexCfgDlg 对话框

class RegexCfgDlg : public CDialogEx
{
	DECLARE_DYNAMIC(RegexCfgDlg)

public:
	RegexCfgDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~RegexCfgDlg();

// 对话框数据
	enum { IDD = IDD_DLG_CFG_REGEX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	
	afx_msg void OnBnClickedButtonAddPattern();
	
	virtual BOOL OnInitDialog();

	int read_config(const char* file,std::vector<patterninfo>& patterns);
	int write_config(const char* file,std::vector<patterninfo>& patterns);
	int show_existing_patterns(CListCtrl& listctrl,std::vector<patterninfo>& patterns);
	int add_pattern(std::vector<patterninfo>& patterns,CListCtrl& listctrl,patterninfo& newpattern);
	int delete_pattern(int item,std::vector<patterninfo>&patterns,CListCtrl& listctrl);
public:
	CListCtrl m_list;
	
	CEdit m_editName;
	CEdit m_editPattern;
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonSelectAll();
	afx_msg void OnBnClickedButtonSelectNone();
	afx_msg void OnBnClickedButtonDelete();
};
