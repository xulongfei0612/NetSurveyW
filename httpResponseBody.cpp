// httpResponseBody.cpp : 实现文件
//

#include "stdafx.h"
#include "NetSurveyW.h"
#include "httpResponseBody.h"
#include "afxdialogex.h"


// httpResponseBody 对话框

IMPLEMENT_DYNAMIC(httpResponseBody, CDialogEx)

httpResponseBody::httpResponseBody(CWnd* pParent /*=NULL*/)
	: CDialogEx(httpResponseBody::IDD, pParent)
{

}

httpResponseBody::~httpResponseBody()
{
}

void httpResponseBody::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_EDIT_RES_BODY, m_edit);
}


BEGIN_MESSAGE_MAP(httpResponseBody, CDialogEx)

	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// httpResponseBody 消息处理程序


BOOL httpResponseBody::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}




void httpResponseBody::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
	CRect   rec;
	GetClientRect(&rec);
	m_edit.MoveWindow(rec);
	// TODO: 在此处添加消息处理程序代码
}
