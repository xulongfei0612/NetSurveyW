#pragma once


// CExListBox

class CExListBox : public CListBox
{
	DECLARE_DYNAMIC(CExListBox)

public:
	CExListBox();
	virtual ~CExListBox();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};


