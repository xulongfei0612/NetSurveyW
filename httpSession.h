#ifndef _HTTP_SESSION_H
#define _HTTP_SESSION_H
#include<map>
#include<vector>
#include <time.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<iostream>
#include "httpUtil.h"
#include <Winsock2.h>
#include "Pcre.h"

#include<fstream>
#include "msgdefine.h"
#include "outputhttp.h"

#define  backupfile    "./data/temp.data"



class httpSession
{
public:
	httpSession(void);
	~httpSession(void);
	int init();
	int create_session(Ntuple4& key);
	int update_session(Ntuple4& key,const char* data,int datalen,int dataType);
	int destroy_session(Ntuple4& key);
	int fini();

	void register_session_callback(SESSION_CALLBACK p);
	void register_interaction_callback(INTERACTION_CALLBACK p);
	int clear();
private:
	int create_interaction(http_interaction** hi);
	int create_request(httprequest** req);
	int create_response(httpresponse** rep);
	int destroy_interaction(http_interaction* hi);
	int destroy_request(httprequest* req);
	int destroy_response(httpresponse* rep);

	int is_one_new_request(const char* data,int datalen);
	int my_uuid_generate(char out[],int len);
	int is_first_response(const char* data,int dataLen);

	/*string glue_original_kvalue_str(const char* url,const char* cookie,const char*  host
		,const char*  requestMethod,const char* postpayload,const char* responsecode );*/

	/*int insert_value_into_requesttable(httprequest* req,http_session* session);
	int insert_value_into_responsetable(httpresponse* rep);*/


	void print_interaction(struct http_interaction* one);
	void delete_session_of_map();
private:

	std::map<Ntuple4,struct http_session*> _session_table;
	httpUtil _httputil;
	Pcre _pcre;
	outputhttp out;
	SESSION_CALLBACK _callback;
	INTERACTION_CALLBACK _inter_callback;
};
#endif
